/* mimir-cli.c - Mimir card sorting test
 *
 * Copyright (C) 2017 Jens Sauer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <termios.h>
#include <libmimir.h>

static int run_assessment(struct mimir *asmt);
static void print_screen(const char *reference_cards, const char *stimulus,
		const int response);
static int get_input(void);
static int get_command(void);
static int want_save_assessment(void);
static int save_assessment(const struct mimir *asmt);
static int get_password(char *buf, const size_t buf_len);
static int sanitize_input(char *in);
static void flush_stdin(void);
static char *make_absolute(char *buf, const size_t buf_len);
static int path_is_absolute(const char *filename);
static int want_override(const char *path);
static void prompt_filename(char *buf, const size_t buf_len);
static void prompt_password(char *buf, const size_t buf_len);
static int want_retry(void);
static int get_participant_id(struct mimir *asmt);
static int verifiy_participant_id(const char *buf);

int main(int agrc, char *argv[])
{
	int ret;
	int command;
	struct mimir *asmt;

	printf("\x1b[1m%s\x1b[0m\n", "M\u00edmir card sorting test");

	while (1) {
		printf("\n\x1b[1;34m%s\x1b[0m",
			"(s)tart new assessment, (q)uit\n");
		command = get_command();
		if (command == 1) {
			printf("Quit M\u00edmir\n");
			break;
		} else if (command == 0) {
			ret = mimir_new(&asmt);
			if (ret) {
				fprintf(stderr,
					"Could not create assessment object\n");
				goto out;
			}
			ret = get_participant_id(asmt);
			if (ret) {
				fprintf(stderr,
				"Could not set participant identifier\n");
				goto out_free_asmt;
			}
			ret = run_assessment(asmt);
			/*
			 * When an error occured in run_assessment
			 * free everything and exit
			 */
			if (ret == 1)
				goto out_free_asmt;
			/*
			 * Clear screen and show assessment is finished
			 */
			printf("\x1b[2J\x1b[1;1H");
			printf("Assessment finished. Continue with any key\n");
			getchar();

			if (ret == 0) {
				if(want_save_assessment())
					save_assessment(asmt);
			}
			mimir_unref(asmt);
		}
	}

	return 0;

out_free_asmt:
	mimir_unref(asmt);
out:
	return EXIT_FAILURE;
}

static int run_assessment(struct mimir *asmt)
{
	struct mimir_card *stimulus;
	char *ref_cards_text;
	char *stimulus_text;
	int ret;
	int err;
	int input;

	ret = mimir_get_ref_cards_text(asmt, &ref_cards_text);
	if (ret) {
		fprintf(stderr, "Could not get reference cards\n");
		err = 1;
		goto out;
	}
	ref_cards_text = mimir_card_text_add_padding(ref_cards_text, 10);
	while ((ret = mimir_get_next_card(asmt, &stimulus)) !=
			-MIMIR_ERR_AFIN) {
		switch (ret) {
		case -MIMIR_ERR_INVAL:
			fprintf(stderr, "Could not get next card: %s\n",
				mimir_err_get_message(ret));
			err = 1;
			goto out_free_card;
		case -MIMIR_ERR_NORSP:
			fprintf(stderr,
			"No response was set, this is propbably a bug\n");
			err = 1;
			goto out_free_card;
		case 0:
		default:
			break;
		}
		ret = mimir_card_get_text(stimulus, &stimulus_text);
		if (ret) {
			fprintf(stderr,
				"Could not get text version of card %s\n",
				mimir_err_get_message(ret));
			err = 1;
			goto out_free_card;
		}
		stimulus_text = mimir_card_text_add_padding(stimulus_text, 35);
		print_screen(ref_cards_text, stimulus_text, 1);

		input = get_input();
		if (input == -1) {
			printf("Quit assessment\n");
			err = -1;
			goto out_free_card_text;
		}

		ret = mimir_add_response(asmt, input, -1);
		if (ret == -MIMIR_ERR_INVAL) {
			fprintf(stderr,
				"Could not set a response, this is a bug\n");
			err = 1;
			goto out_free_card_text;
		}
		print_screen(ref_cards_text, stimulus_text, ret);
		sleep(1);

		free(stimulus_text);
		stimulus_text = NULL;
		mimir_card_unref(stimulus);
		stimulus = NULL;
	};

	free(ref_cards_text);

	return 0;

out_free_card_text:
	free(stimulus_text);
out_free_card:
	mimir_card_unref(stimulus);
	free(ref_cards_text);
out:
	return err;
}

static void print_screen(const char *reference_cards, const char *stimulus,
		const int response)
{
	const char *instructions =
		"Sort the lower card to one of the upper stacks";

	printf("\x1b[2J\x1b[1;1H");
	printf("\n\x1b[1m%*s\x1b[0m\n\n", 40 + (int) strlen(instructions) / 2,
			instructions);
	printf("%s\n%*d%*d%*d%*d\n\n", reference_cards,
		15, 1,
		17, 2,
		17, 3,
		17, 4);
	switch (response) {
	case 1:
		printf("\n\n");
		break;
	case 0:
		printf("\x1b[32m%43s\x1b[0m\n\n", "CORRECT");
		break;
	case -MIMIR_ERR_BADRSP:
		printf("\x1b[31m%44s\x1b[0m\n\n", "INCORRECT");
		break;
	default:
		break;
	}
	printf("%s\n\n", stimulus);
}

static int get_input(void)
{
	int valid = 0;
	int ret;
	int c;

	printf("\x1b[1;34m%s\x1b[0m\n",
		"Which card do you choose [1/a, 2/s, 3/d, 4/f, (q)uit]? ");
	while (valid == 0) {
		c = getchar();
		switch (c) {
		case '1':
		case 'a':
		case 'A':
			ret = MIMIR_ASMT_REF_CARD_ONE;
			valid = 1;
			break;
		case '2':
		case 's':
		case 'S':
			ret = MIMIR_ASMT_REF_CARD_TWO;
			valid = 1;
			break;
		case '3':
		case 'd':
		case 'D':
			ret = MIMIR_ASMT_REF_CARD_THREE;
			valid = 1;
			break;
		case '4':
		case 'f':
		case 'F':
			ret = MIMIR_ASMT_REF_CARD_FOUR;
			valid = 1;
			break;
		case 'q':
		case 'Q':
			ret = -1;
			valid = 1;
			break;
		default:
			printf("Invalid input, please retry\n");
			break;
		}
		flush_stdin();
	};

	return ret;
}

static int get_command(void)
{
	int c;
	int ret;
	int valid = 0;

	while (valid == 0) {
		c = getchar();
		switch (c) {
		case 's':
		case 'S':
			valid = 1;
			ret = 0;
			break;
		case 'q':
		case 'Q':
			valid = 1;
			ret= 1;
			break;
		case '\n':
			continue;
		default:
			printf("Unknown command %c\n", c);
			break;
		}
		flush_stdin();
	}

	return ret;
}

static int want_save_assessment(void)
{
	int c;
	int ok;

	do {
		printf("Save assessment? (y/n): ");
		c = getchar();
		switch (c) {
		case 'y':
		case 'Y':
			ok = 1;
			break;
		case 'n':
		case 'N':
			ok = 0;
			break;
		default:
			ok = 2;
			break;
		}
		flush_stdin();
	} while (ok == 2);

	return ok;
}

static int save_assessment(const struct mimir *asmt)
{
	char filename[200];
	char password[200];
	int ret;

	prompt_filename(filename, sizeof(filename));
	prompt_password(password, sizeof(password));

	do {
		if (!path_is_absolute(filename))
			make_absolute(filename, sizeof(filename));
		if (access(filename, F_OK) == 0)
			ret = want_override(filename);
		else
			ret = 1;
		if (ret == 0)
			prompt_filename(filename, sizeof(filename));
	} while (ret == 0);

	ret = mimir_save(asmt, filename, password, 0);
	if (ret) {
		fprintf(stderr, "Could not save assessment: %s\n",
				mimir_err_get_message(ret));
		if (want_retry())
			ret = save_assessment(asmt);
		else
			ret = 0;
	}
	return ret;
}

static void prompt_filename(char *buf, const size_t buf_len)
{
	int ok;
	int ret;

	do {
		memset(buf, 0, buf_len);
		printf("Filename: ");
		fgets(buf, buf_len, stdin);
		ret = sanitize_input(buf);
		switch (ret) {
		case 1:
			fprintf(stderr, "Filename was to long. %lu maximum\n",
					buf_len);
			ok = 0;
			break;
		case -1:
			ok = 0;
			break;
		case 0:
			ok = 1;
			break;
		default:
			fprintf(stderr, "Error handling input!\n");
			ok = 0;
			break;
		};
	} while (ok == 0);
}

static void prompt_password(char *buf, const size_t buf_len)
{
	int ok;
	int ret;

	do {
		memset(buf, 0, buf_len);
		printf("Password: ");
		ret = get_password(buf, buf_len);
		if (ret) {
			fprintf(stderr, "Could not get password. Retry");
			ok = 0;
			continue;
		}
		ret = sanitize_input(buf);
		switch (ret) {
		case 1:
			fprintf(stderr, "Password was to long. %lu maximum\n",
					buf_len);
			ok = 0;
			break;
		case -1:
			ok = 0;
			break;
		case 0:
			ok = 1;
			break;
		default:
			fprintf(stderr, "Error handling input!\n");
			ok = 0;
			break;
		};
	} while (ok == 0);
}

static int sanitize_input(char *in)
{
	size_t len;

	len = strlen(in);
	/* Remove single newline character */
	if (len == 1 && in[0] == '\n') {
		in[0] = '\0';
		return -1;
	}
	/* Input exceeded buffer */
	if (len > 1 && (in[len - 1] != '\n')) {
		flush_stdin();
		return 1;
	}
	if (len > 1 && (in[len - 1] == '\n')) {
		in[len - 1] = '\0';
		return 0;
	}
	return -2;
}

static void flush_stdin(void)
{
	int c;

	while ((c = getchar()) != '\n' && c != EOF);

}

static int get_password(char *buf, const size_t buf_len)
{
	struct termios old;
	struct termios new;
	int ret;

	ret = tcgetattr(fileno(stdin), &old);
	if (ret)
		goto out;
	new = old;
	new.c_lflag &= ~ECHO;

	ret = tcsetattr(fileno(stdin), TCSAFLUSH, &new);
	if (ret)
		goto out;
	fgets(buf, buf_len, stdin);

	ret = tcsetattr(fileno(stdin), TCSAFLUSH, &old);
	if (ret)
		goto out;
	return 0;
out:
	return 1;
}

static int path_is_absolute(const char *filename)
{
	if (filename[0] == '/')
		return 1;
	else
		return 0;
}

static char *make_absolute(char *buf, const size_t buf_len)
{
	char cwd[200];
	char *tmp;
	size_t size;

	getcwd(cwd, sizeof(cwd));
	size = strlen(cwd) + strlen(buf) + 2;
	if (size > buf_len) {
		abort();
	}
	tmp = malloc(strlen(cwd) + strlen(buf) + 2);
	strncpy(tmp, cwd, strlen(cwd) + 1);
	strncat(tmp, "/", 1);
	strncat(tmp, buf, strlen(buf));

	memcpy(buf, tmp, size);
	free(tmp);

	return buf;
}

static int want_override(const char *path)
{
	int c;
	int ret;

	do {
		printf("The file %s does exists. Override? (y/n): ", path);
		c = getchar();
		switch (c) {
		case 'y':
		case 'Y':
			ret = 1;
			break;
		case 'n':
		case 'N':
			ret = 0;
			break;
		default:
			ret = -1;
			break;
		};
		flush_stdin();
	} while (ret == -1);

	return ret;
}

static int want_retry(void)
{
	int c;
	int ret;

	do {
		printf("Retry saving assessment? (y/n): ");
		c = getchar();
		switch (c) {
		case 'y':
		case 'Y':
			ret = 1;
			break;
		case 'n':
		case 'N':
			ret = 0;
			break;
		default:
			ret = -1;
			break;
		};
		flush_stdin();
	} while (ret == -1);

	return ret;
}

static int get_participant_id(struct mimir *asmt)
{
	char buf[200];
	int ok = 0;
	size_t len;

	do {
		memset(buf, 0, sizeof(buf));
		printf("Participant identifier (%lu characters)\n",
				sizeof(buf));
		fgets(buf, sizeof(buf), stdin);
		len = strlen(buf);
		if (len == 1)
			continue;
		if (len > 1 && (buf[len - 1] != '\n')) {
			flush_stdin();
			fprintf(stderr,
			"Participant identifier to long. Maximum: %lu\n",
				sizeof(buf));
			continue;
		}
		if (len > 1 && (buf[len - 1] == '\n'))
			buf[len - 1]  = '\0';
		ok = verifiy_participant_id(buf);
	} while (ok == 0);

	ok = mimir_set_pid(asmt, buf);
	if (ok)
		return ok;

	return ok;
}

static int verifiy_participant_id(const char *buf)
{
	char correct;
	int ok;

	printf("Verify participant identifier: %s\n", buf);
	do {
		printf("Correct? y/n: ");
		correct = getchar();
		flush_stdin();

		switch (correct) {
		case 'y':
		case 'Y':
			ok = 1;
			break;
		case 'n':
		case 'N':
			ok = 0;
			break;
		default:
			ok = 2;
			break;
		}
	} while (ok == 2);
	return ok;
}
