|pipeline| |codecov|

.. |pipeline| image:: https://gitlab.com/mimir-project/mimir/badges/master/build.svg
   :alt: Build Status
   :target: https://gitlab.com/mimir-project/mimir/pipelines

.. |codecov| image:: https://codecov.io/gl/mimir-project/mimir/branch/master/graph/badge.svg?token=Js1moVaI3k
   :alt: Code Coverage
   :target:  https://codecov.io/gl/mimir-project/mimir

What is Mímir?
--------------
Mímir is a free implementation of the Berg's Card Sorting Test. Mímir is
a neuropsychology test which helps physicians to test the cognition of a
patient.

Features
--------
Encryption
        Encrypted storage of data, detection of data corruption.

        More information can be found in Section *Data encryption* in
        the *Mímir File Format*.

Status and Portability
----------------------
This programm is currently in beta status, changes to the API and behavior
can happen at any time.

At this time, Mímir is only supported on Linux based OS.
If you want to change this, feel free and add an issue to discuss this.

Links
-----
| `Mímir releases <https://gitlab.com/mimir-project/mimir/releases>`_
| `Git repository <https://gitlab.com/mimir-project/mimir>`_
| `Issue tracker <https://gitlab.com/mimir-project/mimir/issues>`_
| `Latest documentation <https://mimir-project.gitlab.io/doc/latest>`_
| `Older documentation <https://mimir-project.gitlab.io/doc>`_

License
-------
This software is free software and is licensed under the terms of the GPLv3.
See COPYING for details.

The common name 'Wisconsin Card Sorting Test' is a registred trademark of
Wells Print & Digital Services and is not associated with Mímir.
