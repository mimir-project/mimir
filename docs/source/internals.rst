Internals
=========
These documents providing information of the inner workings of Mímir.

.. toctree::
   :maxdepth: 2

   file-format
   internal-api
