Internal API
============
This section lists internal API functions and types. These are only
available for internal code within Mímir and are not exported in the
shared library.

struct mimir
------------
The struct :c:type:`mimir` is the base type in Mímir. After creation all
information of the assessment and internal state are stored in this
struct.

.. doxygenstruct:: mimir
   :no-link:
   :members:

struct mimir_card
-----------------
This opaque struct is used for all cards, stimulus or reference
cards in Mímir. The API to create a card is hidden to the users, because
they do not have to.

The struct :c:type:`mimir_card` has to independant reference couters.
The reason is that struct :c:type:`mimir_card` is exported to the users
and also part of the private struct :c:type:`mimir_card_list`.

.. doxygenstruct:: mimir_card
   :no-link:
   :members:
   :undoc-members:

Functions
^^^^^^^^^
.. doxygenfunction:: card_new()
.. doxygenfunction:: card_ref_private()
.. doxygenfunction:: card_unref_private()

struct mimir_card_list
----------------------
The struct :c:type:`mimir_card_list` is a list of stimulus cards used
for presentation to the participant. The 64 cards are shuffled, that
adjacent cards do not have any equal properties. With
:c:func:`card_list_get_next` the next card can be retrieved from the
stack. If the stack is empty, the cards will be reshuffled
automatically.

.. doxygenstruct:: mimir_card_list
   :members:
   :undoc-members:

Functions
^^^^^^^^^
.. doxygenfunction:: card_list_new
.. doxygenfunction:: card_list_unref
.. doxygenfunction:: card_list_get_next

struct response_list
--------------------
The struct :c:type:`response_list` is a linked list for the responses
of the participant.

.. doxygenstruct:: response_list
   :members:
   :undoc-members:
.. doxygenstruct:: response_node
   :members:
   :undoc-members:
.. doxygenstruct:: response_data
   :members:
   :undoc-members:

Functions
^^^^^^^^^
.. doxygenfunction:: response_list_new
.. doxygenfunction:: response_list_free
.. doxygenfunction:: response_list_append
.. doxygenfunction:: response_list_append_data
.. doxygenfunction:: response_list_nodes
.. doxygenfunction:: response_list_walk
.. doxygenfunction:: response_data_hash
.. doxygenfunction:: response_data_hton
.. doxygenfunction:: response_data_ntoh

Debug messages
---------------
This macro can be used to print debug messages with printf-like style
to ``stderr``. When Mímir was compiled with debug disabled, these messages
will not be compiled in the binary.

You must pass the environment variable ``MIMIR_DEBUG=1`` to enable debug
messages.

.. doxygendefine:: debug

Random numbers
--------------
Good random numbers are required by Mímir. They are used for salt
generation, shuffleing of cards and maybe more.
The best way to get crypthological safe pseudo random numbers is to get them
from the operation system instead from userspace.

The macro get_random_bytes() will be replaced with a function to the
current OS CSPRNG.

.. note::
        Currently only Linux, 3.17 and later, are supported. Feel free
        and create a merge request.

.. doxygendefine:: get_random_bytes

Encryption
----------
These are the internal functions for derive a key with PBKDF2 and
encrypt or decrypt data. These are wrappers around OpenSSL (libcrypto) API.

.. doxygendefine:: MIMIR_KEY_BYTES
.. doxygendefine:: MIMIR_TAG_BYTES
.. doxygenfunction:: derive_key()
.. doxygenfunction:: encrypt()
.. doxygenfunction:: decrypt()

UUID
----
Universally Unique IDentifier (UUID) are used to identify assessments
and partcipants. Mímir has its own UUID version 4 and 5 implementation,
compliant to RFC 4122 (https://tools.ietf.org/html/rfc4122).

For the version 5 UUID a custom namespace must be used, which is
accessable at :c:data:`mimir_uuid_namespace`.

.. doxygenstruct:: mimir_uuid
   :undoc-members:
.. doxygenvariable:: mimir_uuid_namespace
.. doxygenfunction:: uuid_v4
.. doxygenfunction:: uuid_v5
.. doxygenfunction:: uuid_string
.. doxygenfunction:: uuid_from_string
.. doxygenfunction:: uuid_hton
.. doxygenfunction:: uuid_ntoh
