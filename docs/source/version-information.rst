Version Checking Macros
=======================
libmimir provides macros to check the version of the library at compile-time

.. doxygenfile:: libmimir-version.h
