Implementation Details
======================

This document explains the libmimir implementation
of the 'Berg's Card Sorting Test'

.. _cards-label:

Stimulus Cards
--------------

64 stimulus cards are used, which have a unique combination
of *form*, *color* and *number*.
Following items are used:

* **Form**: triangle, star, cross or circle
* **Color**: red, green, yellow or blue
* **Number**: One, two, three or four

The cards are shuffled that no adjacent cards have the same items.

Reference Cards
---------------

Following reference cards are used:

* One red triangle
* Two green stars
* Three yellow crosses
* Four blue circles

Use of other reference cards is maybe planed for the future.

Sorting Principle
-----------------

The order of sorting criteria is: Color, form, number.
After 10 consecutive correct matches the principle will changed to the next.

Random sort criteria maybe planed for the future.

End Of Test
-----------

The Test ends when one of the following criteria is met:

* Two decks of cards (128) are used
* Two sequences of sorting are completed

Measurements
------------

Following measurements are planned for the final version.

Trials to Complete First Category
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Number of trials to completed the first category

Categories Completed
^^^^^^^^^^^^^^^^^^^^
Total number of completed sort principles.

Failure to Maintain Set
^^^^^^^^^^^^^^^^^^^^^^^
Number referes to the inability to complete the sort principle after 5 or
more consecutive correct responses

Conceptual Level Responses
^^^^^^^^^^^^^^^^^^^^^^^^^^
Consecutive correct responses of three or more.

Percent Conceptual Level Responses
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Conceptual level responses in percent of total trials.

Learning to Learn
^^^^^^^^^^^^^^^^^
Indicates the participant's "average change in conceptual efficiency across
the the consecutive categories"

TODO: Research algorithm

Total Error
^^^^^^^^^^^
Total number of wrong responses. Includes perseverative and
nonperseverative erors.

Perseverative Errors
^^^^^^^^^^^^^^^^^^^^
The number of errors where the participant has used the same rule
for their choice as the previous choice

Nonperseverative Errors
^^^^^^^^^^^^^^^^^^^^^^^
Incorrect responses other than the perseverative errors.

References
----------

#. Berg, E. A. (1948). A simple objective technique for measuring flexibility in thinking. J. Gen. Psychol. 39: 15-22.
#. Mitrushina, M., Boone, K. B., Razani, J., & D'Elia, L. F. (2005). Handbook of normative data for neuropsychological assessment. Oxford University Press.
#. Grant, D. A., & Berg, E. (1948). A behavioral analysis of degree of reinforcement and ease of shifting to new responses in a Weigl-type card-sorting problem. Journal of experimental psychology, 38(4), 404.
