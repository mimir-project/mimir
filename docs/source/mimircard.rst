Mimir Card Class
================
The *mimir_card* class is used to represent a card in libmimir.
They are used as reference cards and stimulus cards for the proband.

See more information about cards in libmimir: :ref:`cards-label`

Data Structures
---------------
.. doxygenstruct:: mimir_card

Properties
----------
.. doxygenenum:: mimir_card_property

Functions
---------
.. doxygenfunction:: mimir_card_ref
.. doxygenfunction:: mimir_card_unref
.. doxygenfunction:: mimir_card_get_form
.. doxygenfunction:: mimir_card_get_color
.. doxygenfunction:: mimir_card_get_number
.. doxygenfunction:: mimir_card_get_props
.. doxygenfunction:: mimir_card_get_prop_raw
.. doxygenfunction:: mimir_card_raw_to_color
.. doxygenfunction:: mimir_card_raw_to_number
.. doxygenfunction:: mimir_card_raw_to_form
.. doxygenfunction:: mimir_card_have_equal_prop
.. doxygenfunction:: mimir_card_prop_str
.. doxygenfunction:: mimir_card_get_text
.. doxygenfunction:: mimir_card_text_add_padding
