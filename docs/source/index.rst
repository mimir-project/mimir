Mímir documentation
===================
.. include:: ../../README.rst

.. toctree::
   :maxdepth: 2

   implementation
   reference
   internals
