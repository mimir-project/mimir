Mímir File Format
=================
This document describes how data from Mimir is stored on the disk.
Multi-byte formats must be converted into Big-Endian (Network Byte Order).

Identification
--------------
To identify Mimir file format, the first 8 bytes are: ::

(decimal)               137  77  73  77  13  10  26  10
(hexadecimal)            89  4d  49  4d  0d  0a  1a  0a
(ASCII C notation)     \211   M   I   M  \r  \n \032 \n

Header
------
The header starts at offset 8 and contains the following fields:

File format version
^^^^^^^^^^^^^^^^^^^
1 byte version number **offset 0x8**.
The current version is 1 (dec).

Password salt
^^^^^^^^^^^^^
32 byte of password salt **offset 0x0a**

The password salt increases the user defined password entropy by adding random
32 byte of data to the password.

Password iteration
^^^^^^^^^^^^^^^^^^
Argument for the PBKDF2 password hash function.

Assessment UUID
^^^^^^^^^^^^^^^
UUID version 4 of the assessment. This can be used as an unique
identifer.

Participant UUID
^^^^^^^^^^^^^^^^
UUID version 5 of the participant identifier.

Summary
^^^^^^^
The file header contains follow fields: ::

+00 8B  Magic number (0x894d494d0d0a1a0a)
+08 1B  File format version
+09 32B Password salt
+29 4B  Password iteration
+2d 16B Assessment UUID
+3d 16B Participant UUID
+4d ciphertext

Data encryption
---------------
Mímir uses OpenSSL version 1.1.0 or later for the encryption. It does
not implement the routines itself. Mímir is only linked against
``libcrypto`` for low-level encryption and hash implementations. It does
not need ``libssl``, Mímir does not have any network connectivity.

Mímir uses the stream cipher ChaCha20 with Poly1305 as an message
authentication code. This will provide authenticated encryption with
associated data (AEAD) for Mímir data. This provides:

 * Confidentiality by encryption
 * Authenticity by MAC
 * Authenticity for associated data, like the unencrypted header

This means Mímir data will be encrypted and file corruption or
alteration on purpose can detected.

For more information on ChaCha20-Poly see:
`RFC 7539 <https://tools.ietf.org/html/rfc7539>`_

Data chunks
-----------
Each data chunk has a size, chunk type, data and hash field.

* 4 bytes unsigned int length of data. The length only counts the data field.
* 4 bytes of chunk type identifier.
* Chunk data

::

  |----------------------|
  | Length | Type | Data |
  |----------------------|

Valid type identifier are upper case letters from A-Z.

Chunk specifications
--------------------
Participant identifier
^^^^^^^^^^^^^^^^^^^^^^
User choosen participant identifier.

Chunk type is: ``IDPA`` decimal: 73 68 80 65

The ``IDPA`` chunk contains a NULL terminated string. String length is equal
the data length.

Assessment response data
^^^^^^^^^^^^^^^^^^^^^^^^
This chunk holds the response of the participant, the assessment itself.

Chunk type is: ``ASMT`` decimal: 65 83 77 84

The ``ASMT`` chunk contains: ::

  32 bytes hash of response
  32 bytes parent response hash
  4 bytes reference card raw value
  4 bytes stimulus card raw value
  4 bytes sorting principle
  4 bytes correct answer (value > 0 means true)

The parent hash is calculated on the actual chunk data, do not mistake with
chunk hash. The parent hash is used to create a list of responses, whereas the
chunk hash is used to provide data integrety.

Assessment state
^^^^^^^^^^^^^^^^
It is important to know if the assessment is finished on saving or not.
The chunk type ``AFIN`` decimal: ``65 70 73 78`` hex: ``0x41 0x46 0x49
0x4e`` holds this information.

The ``AFIN`` chunk contains: ::

  4 byte assessment state indicator

The indicator is ``0`` when the assessment is unfinished and ``!= 0``
when the assessment is finished.

Assessment time
^^^^^^^^^^^^^^^
Calendar time (UTC) in seconds. Unix epoch time.

Chunk type is: ``TIME`` decimal: 84 73 77 69

The ``TIME`` chunk contains: ::

 8 byte unix epoch (long int)

Planned chunk specifications
----------------------------
 * Comment field for examiner
 * UUID of assessment, proband and examiner
 * Cooperation and effort of proband
