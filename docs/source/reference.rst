API Reference
=============
Documentation of the public API can be found here.

.. toctree::
   :maxdepth: 2

   mimir
   mimircard
   errors
   mimir_time
   version-information
