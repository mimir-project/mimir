Documentation
=============
We use doxygen, breathe and sphinx to create the documentation.  
Please consult the manuals how to use them.
- http://www.stack.nl/~dimitri/doxygen/manual/index.html
- https://breathe.readthedocs.io/en/latest/
- http://www.sphinx-doc.org/en/stable/

Breathe directives
------------------
These directives allow to import doxygen documentation into sphinx rst files.
https://breathe.readthedocs.io/en/latest/directives.html

`.. doxygenfile:: <file>` Will import all doxygen documentation from file.

Inline reStructuredText Markup
-----------------
To use RST inside a block of comments, sourround the markup with `\rst` and
`\endrst`.
Leading asterisk of comment blocks are ignored.

Public API
---------------------
Document all public API methods, types, etc. Documentation should be written on
definition. Keep most documentation inside .c files.

Private API
---------------------
At this point it is not clear how we document private API functions. Maybe a
doxygen group which collects all private API component can be helpfull.

Generate documentation
----------------------
You can get the required software by running `pip install -r requirements.txt`  
Run `configure` with the `--enable-doc` flag. The make target `htmldoc` will
generate the html documentation.
