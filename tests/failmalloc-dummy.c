/* failmalloc-dummy.c
 *
 * Provide the dummy library to satisfiy the linker. The real
 * malloc_dbg_set_count() will be supplied by failmalloc.c and must
 * be interposed by LD_PRELOAD
 *
 * Copyright (C) 2017 Jens Sauer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include "failmalloc.h"

void malloc_dbg_set_count(const unsigned int count)
{
	fprintf(stderr, "Run test with LD_PRELOAD=libfailmalloc.so\n");
	abort();
}
