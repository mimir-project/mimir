/* failmalloc.h
 *
 * Provide the library to interpose memory allocation function.
 * The library must be added by LD_PRELOAD to the linker.
 * malloc_dbg_set_count() is used so set the count at which the allocation
 * should fail. To have normal behaviour, set the count to 0.
 *
 * Copyright (C) 2017 Jens Sauer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

void malloc_dbg_set_count(const unsigned int count);
