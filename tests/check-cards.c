/* check-cards.c
 *
 * Copyright (C) 2017 Jens Sauer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libmimir.h>
#include <stdio.h>
#include <stdlib.h>
#include <check.h>
#include "libmimir-private.h"

Suite *card_suite(void);

START_TEST(test_card_create)
{
	struct mimir_card *c;
	unsigned int f;
	unsigned int n;
	unsigned int co;

	card_new(&c, MIMIR_CARD_PROP_STAR | MIMIR_CARD_PROP_RED |
			MIMIR_CARD_PROP_TWO);
	ck_assert_int_eq(mimir_card_get_form(c), MIMIR_CARD_PROP_STAR);
	ck_assert_int_eq(mimir_card_get_color(c), MIMIR_CARD_PROP_RED);
	ck_assert_int_eq(mimir_card_get_number(c), MIMIR_CARD_PROP_TWO);

	mimir_card_get_props(c, &f, &co, &n);
	ck_assert_int_eq(f, MIMIR_CARD_PROP_STAR);
	ck_assert_int_eq(co, MIMIR_CARD_PROP_RED);
	ck_assert_int_eq(n, MIMIR_CARD_PROP_TWO);

	ck_assert_int_eq(mimir_card_get_props(c, &f, NULL, &n), -1);
	ck_assert_int_eq(mimir_card_get_props(NULL, NULL, NULL, NULL), -1);
	ck_assert_int_eq(mimir_card_get_props(c, NULL, &co, &n), -1);
	ck_assert_int_eq(mimir_card_get_props(c, &f, &co, NULL), -1);

	card_unref_private(c);
}
END_TEST

START_TEST(test_get_props_from_null)
{
	ck_assert_int_eq(mimir_card_get_form(NULL), -1);
	ck_assert_int_eq(mimir_card_get_color(NULL), -1);
	ck_assert_int_eq(mimir_card_get_number(NULL), -1);
}
END_TEST

START_TEST(test_equal)
{
	struct mimir_card *c1;
	struct mimir_card *c2;

	if (card_new(&c1, 0x888) != 0)
		ck_abort();
	if (card_new(&c2, 0x111) != 0)
		ck_abort();

	ck_assert_int_eq(mimir_card_have_equal_prop(c1, c2), 0);
	ck_assert_int_eq(mimir_card_have_equal_prop(c1, NULL), 0);
	ck_assert_int_eq(mimir_card_have_equal_prop(NULL, c1), 0);

	card_unref_private(c2);

	ck_assert_int_eq(mimir_card_have_equal_prop(c1, c1), 1);
	card_unref_private(c1);
}
END_TEST

START_TEST(adjacent_card_props)
{
	struct mimir *testdata;
	struct mimir_card **cards;

	for (int k = 0; k < 100; k++) {
		if (mimir_new(&testdata))
			ck_abort();
		cards = testdata->cards->list;

		for (int i = 0; i < MIMIR_N_CARDS - 1; i++) {
			ck_assert_int_eq(mimir_card_have_equal_prop
					 (cards[i], cards[i + 1]), 0);
		}
		mimir_unref(testdata);
	}
}
END_TEST

static void invalid_flags_card(unsigned int flags)
{
	struct mimir_card *c;

	ck_assert_int_eq(-MIMIR_ERR_INVAL, card_new(&c, flags));
}

START_TEST(no_flags)
{
	invalid_flags_card(0);
}
END_TEST

START_TEST(only_color_flag)
{
	invalid_flags_card(MIMIR_CARD_PROP_RED);
}
END_TEST

START_TEST(only_number_flag)
{
	invalid_flags_card(MIMIR_CARD_PROP_TWO);
}
END_TEST

START_TEST(only_form_flag)
{
	invalid_flags_card(MIMIR_CARD_PROP_STAR);
}
END_TEST

START_TEST(missing_form)
{
	invalid_flags_card(MIMIR_CARD_PROP_TWO | MIMIR_CARD_PROP_RED);
}
END_TEST

START_TEST(missing_number)
{
	invalid_flags_card(MIMIR_CARD_PROP_CIRCLE | MIMIR_CARD_PROP_RED);
}
END_TEST

START_TEST(missing_color)
{
	invalid_flags_card(MIMIR_CARD_PROP_TWO | MIMIR_CARD_PROP_FOUR);
}
END_TEST

START_TEST(multiple_color_flags)
{
	invalid_flags_card(MIMIR_CARD_PROP_RED | MIMIR_CARD_PROP_ONE |
			   MIMIR_CARD_PROP_CIRCLE | MIMIR_CARD_PROP_BLUE);
}
END_TEST

START_TEST(multiple_number_flags)
{
	invalid_flags_card(MIMIR_CARD_PROP_CROSS | MIMIR_CARD_PROP_YELLOW |
			   MIMIR_CARD_PROP_TWO | MIMIR_CARD_PROP_FOUR);
}
END_TEST

START_TEST(multiple_form_flags)
{
	invalid_flags_card(MIMIR_CARD_PROP_THREE | MIMIR_CARD_PROP_GREEN |
			   MIMIR_CARD_PROP_STAR | MIMIR_CARD_PROP_TRIANGLE);
}
END_TEST

START_TEST(every_flag_multiple)
{
	invalid_flags_card(MIMIR_CARD_PROP_ONE | MIMIR_CARD_PROP_TWO |
			   MIMIR_CARD_PROP_BLUE | MIMIR_CARD_PROP_RED |
			   MIMIR_CARD_PROP_STAR | MIMIR_CARD_PROP_CROSS);
}
END_TEST

START_TEST(outside_boundaries)
{
	invalid_flags_card(0x1248);
}
END_TEST

START_TEST(flag_str)
{
	ck_assert_str_eq("One", mimir_card_prop_str(MIMIR_CARD_PROP_ONE));
	ck_assert_str_eq("Two", mimir_card_prop_str(MIMIR_CARD_PROP_TWO));
	ck_assert_str_eq("Three", mimir_card_prop_str(MIMIR_CARD_PROP_THREE));
	ck_assert_str_eq("Four", mimir_card_prop_str(MIMIR_CARD_PROP_FOUR));

	ck_assert_str_eq("Red", mimir_card_prop_str(MIMIR_CARD_PROP_RED));
	ck_assert_str_eq("Blue", mimir_card_prop_str(MIMIR_CARD_PROP_BLUE));
	ck_assert_str_eq("Green", mimir_card_prop_str(MIMIR_CARD_PROP_GREEN));
	ck_assert_str_eq("Yellow", mimir_card_prop_str(MIMIR_CARD_PROP_YELLOW));

	ck_assert_str_eq("Cross", mimir_card_prop_str(MIMIR_CARD_PROP_CROSS));
	ck_assert_str_eq("Star", mimir_card_prop_str(MIMIR_CARD_PROP_STAR));
	ck_assert_str_eq("Circle", mimir_card_prop_str(MIMIR_CARD_PROP_CIRCLE));
	ck_assert_str_eq("Triangle",
			 mimir_card_prop_str(MIMIR_CARD_PROP_TRIANGLE));

	ck_assert_str_eq("Unknown property", mimir_card_prop_str(99));
}
END_TEST

START_TEST(ref_count)
{
	struct mimir_card *card;
	struct mimir_card *reference;

	card_new(&card, MIMIR_CARD_PROP_ONE | MIMIR_CARD_PROP_BLUE |
			MIMIR_CARD_PROP_STAR);

	/* Increase public ref count */
	reference = mimir_card_ref(card);

	ck_assert_ptr_eq(card, reference);

	/* Decrease private ref count */
	card_unref_private(card);

	ck_assert_int_eq(MIMIR_CARD_PROP_BLUE, mimir_card_get_color(reference));

	/* Increase private ref count */
	card = card_ref_private(reference);

	/* Multiple decrease private count */
	card_unref_private(card);
	card_unref_private(card);	/* Potentially dangerous */

	ck_assert_int_eq(MIMIR_CARD_PROP_BLUE, mimir_card_get_color(reference));

	/* Decrease public ref count */
	mimir_card_unref(reference);
}
END_TEST

START_TEST(ref_count_multiple_unref)
{
	struct mimir_card *card;
	struct mimir_card *reference;

	card_new(&card, MIMIR_CARD_PROP_ONE | MIMIR_CARD_PROP_BLUE |
			MIMIR_CARD_PROP_STAR);

	reference = mimir_card_ref(card);

	mimir_card_unref(reference);
	mimir_card_unref(reference);	/* Potenially invalid mimir_card */

	card_unref_private(card);
}
END_TEST

START_TEST(ref_count_null)
{
	struct mimir_card *card;
	struct mimir_card *reference;

	card_new(&card, MIMIR_CARD_PROP_ONE | MIMIR_CARD_PROP_BLUE |
			MIMIR_CARD_PROP_STAR);

	reference = mimir_card_ref(card);

	mimir_card_unref(reference);
	reference = NULL;
	mimir_card_unref(reference);	/* Should be safe */

	card_unref_private(card);
	card = NULL;
	card_unref_private(card);	/* Should be safe */
}
END_TEST

START_TEST(ref_null)
{
	struct mimir_card *card = NULL;

	mimir_card_ref(card);
	card_ref_private(card);

	card_unref_private(card);
	mimir_card_unref(card);
}
END_TEST

START_TEST(raw_properties)
{
	struct mimir_card *card;
	unsigned int properties_set;
	int raw_properties;
	int err;

	properties_set = MIMIR_CARD_PROP_ONE | MIMIR_CARD_PROP_RED |
			 MIMIR_CARD_PROP_CIRCLE;
	err = card_new(&card, properties_set);
	if (err)
		ck_abort();
	raw_properties = mimir_card_get_prop_raw(card);
	card_unref_private(card);
	ck_assert_int_ne(0, raw_properties);
	ck_assert_int_eq(properties_set, raw_properties);
	ck_assert_int_eq(MIMIR_CARD_PROP_ONE,
			 mimir_card_raw_to_number(raw_properties));
	ck_assert_int_eq(MIMIR_CARD_PROP_RED,
			 mimir_card_raw_to_color(raw_properties));
	ck_assert_int_eq(MIMIR_CARD_PROP_CIRCLE,
			 mimir_card_raw_to_form(raw_properties));

	ck_assert_int_eq(0, mimir_card_get_prop_raw(NULL));
	ck_assert_int_eq(0, mimir_card_raw_to_form(0));
	ck_assert_int_eq(0, mimir_card_raw_to_color(0));
	ck_assert_int_eq(0, mimir_card_raw_to_number(0));
	ck_assert_int_eq(0, mimir_card_raw_to_form(0x337));
}
END_TEST

START_TEST(test_card_list)
{
	struct mimir_card_list *list;
	struct mimir_card *card;
	int ret;

	ret = card_list_new(&list);
	if (ret)
		goto out;
	ck_assert_int_eq(1, list->shuffle_count);
	for (int i = 0; i < MIMIR_N_CARDS + 1; i++) {
		card = card_list_get_next(list);
		mimir_card_unref(card);
	}
	ck_assert_int_eq(2, list->shuffle_count);
	card_list_unref(list);
	list = NULL;
	card_list_unref(list);

	return;
out:
	ck_abort();
}
END_TEST

START_TEST(test_text)
{
	struct mimir_card *card;
	char *text_card;
	char *tmp;
	int err;

	err = card_new(&card, MIMIR_CARD_PROP_ONE |
		MIMIR_CARD_PROP_BLUE| MIMIR_CARD_PROP_CIRCLE);
	if (err)
		goto out;
	err = mimir_card_get_text(card, &text_card);
	if (err)
		goto out_free_card;
	printf("%s\n", text_card);
	ck_assert_ptr_ne(NULL, text_card);
	tmp = strdup(text_card);
	text_card = mimir_card_text_add_padding(text_card, 0);
	ck_assert_int_eq(0, strcmp(tmp, text_card));
	free(tmp);
	free(text_card);
	text_card = NULL;
	err = mimir_card_get_text(card, NULL);
	ck_assert_int_eq(-MIMIR_ERR_INVAL, err);
	err = mimir_card_get_text(NULL, &text_card);
	ck_assert_int_eq(-MIMIR_ERR_INVAL, err);
	text_card = mimir_card_text_add_padding(text_card, 8);
	ck_assert_ptr_eq(NULL, text_card);
	ck_assert_ptr_eq(text_card, mimir_card_text_add_padding(text_card,
		0));

	card_unref_private(card);
	return;

out_free_card:
	card_unref_private(card);
out:
	ck_abort();
}
END_TEST

/*
 * Test if card adjacent cards in the list have any equal properties
 */
START_TEST(card_list_adjacent)
{
	struct mimir_card_list *list;
	struct mimir_card *card;
	struct mimir_card *last_card;
	int err;

	card = NULL;
	last_card = NULL;

	err = card_list_new(&list);
	if (err)
		goto out;

	for (int i = 0; i < MIMIR_N_CARDS * 128; i++) {
		card = card_list_get_next(list);
		/*
		 * Reset last_card everytime the card stack is empty.
		 * When the card stack is empty, the cards are reshuffled,
		 * this can cause that the last card presented has equal
		 * properties. It is not defined if this is a problem.
		 */
		if (i % MIMIR_N_CARDS == 0)
			last_card = NULL;

		ck_assert_int_eq(0, mimir_card_have_equal_prop(card,
				last_card));
		last_card = card;
	}
	card_list_unref(list);

	return;
out:
	ck_abort();
}
END_TEST

Suite *card_suite(void)
{
	Suite *s;
	TCase *tc_core;
	TCase *tc_null_props;
	TCase *tc_equal;
	TCase *tc_flags;
	TCase *tc_list;

	s = suite_create("Cards");

	tc_core = tcase_create("Core");
	tc_null_props = tcase_create("Props from null");
	tc_equal = tcase_create("Test equal function");
	tc_flags = tcase_create("Flags");
	tc_list = tcase_create("Card list");

	tcase_add_test(tc_core, test_card_create);
	tcase_add_test(tc_core, adjacent_card_props);
	tcase_add_test(tc_core, ref_count);
	tcase_add_test(tc_core, ref_count_multiple_unref);
	tcase_add_test(tc_core, ref_count_null);
	tcase_add_test(tc_core, ref_null);
	tcase_add_test(tc_core, test_text);
	suite_add_tcase(s, tc_core);

	tcase_add_test(tc_null_props, test_get_props_from_null);
	suite_add_tcase(s, tc_null_props);

	tcase_add_test(tc_equal, test_equal);
	suite_add_tcase(s, tc_equal);

	tcase_add_test(tc_flags, no_flags);
	tcase_add_test(tc_flags, missing_color);
	tcase_add_test(tc_flags, missing_form);
	tcase_add_test(tc_flags, missing_number);
	tcase_add_test(tc_flags, only_color_flag);
	tcase_add_test(tc_flags, only_form_flag);
	tcase_add_test(tc_flags, only_number_flag);
	tcase_add_test(tc_flags, multiple_color_flags);
	tcase_add_test(tc_flags, multiple_form_flags);
	tcase_add_test(tc_flags, multiple_number_flags);
	tcase_add_test(tc_flags, every_flag_multiple);
	tcase_add_test(tc_flags, outside_boundaries);
	tcase_add_test(tc_flags, flag_str);
	tcase_add_test(tc_flags, raw_properties);
	suite_add_tcase(s, tc_flags);

	tcase_add_test(tc_list, test_card_list);
	tcase_add_test(tc_list, card_list_adjacent);
	suite_add_tcase(s, tc_list);

	return s;
}

int main(int argc, char *argv[])
{
	int number_failed;
	Suite *s;
	SRunner *sr;

	s = card_suite();
	sr = srunner_create(s);

	srunner_run_all(sr, CK_NORMAL);
	number_failed = srunner_ntests_failed(sr);
	srunner_free(sr);

	return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
