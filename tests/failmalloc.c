/* failmalloc.c
 *
 * Provide the library to interpose memory allocation function.
 * The library must be added by LD_PRELOAD to the linker.
 * malloc_dbg_set_count() is used so set the count at which the allocation
 * should fail. To have normal behaviour, set the count to 0.
 *
 * Copyright (C) 2017 Jens Sauer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE
#include <dlfcn.h>
#include <stdlib.h>
#include "failmalloc.h"

__thread unsigned int malloc_count = 0;
static void *real_calloc_dummy(size_t nmemb, size_t size);

void malloc_dbg_set_count(const unsigned int count)
{
	malloc_count = count;
}

void *malloc(size_t size)
{
	static void *(*real_malloc)(size_t) = NULL;

	if (!real_malloc)
		real_malloc = dlsym(RTLD_NEXT, "malloc");
	if (malloc_count) {
		if (--malloc_count == 0)
			return NULL;
	}
	return real_malloc(size);
}

void *calloc(size_t nmemb, size_t size)
{
	static void *(*real_calloc)(size_t, size_t) = NULL;

	if (!real_calloc) {
		/*
		 * temporary set real_calloc to dummy function,
		 * because dlsym calls itself calloc and this will
		 * end in an infinite recursion. dlsym handles NULL
		 * from calloc without a problem
		 */
		real_calloc = real_calloc_dummy;
		real_calloc = dlsym(RTLD_NEXT, "calloc");
	}
	if (malloc_count) {
		if (--malloc_count == 0)
			return NULL;
	}
	return real_calloc(nmemb, size);
}

void *realloc(void *ptr, size_t size)
{
	static void *(*real_realloc)(void*, size_t) = NULL;

	if (!real_realloc)
		real_realloc = dlsym(RTLD_NEXT, "realloc");
	if (malloc_count) {
		if (--malloc_count == 0)
			return NULL;
	}
	return real_realloc(ptr, size);
}

static void *real_calloc_dummy(size_t nmemb, size_t size)
{
	return NULL;
}
