AC_PREREQ([2.69])


dnl ***********************************************************************
dnl Define Versioning Information
dnl ***********************************************************************
m4_define([major_version],[0])
m4_define([minor_version],[4])
m4_define([micro_version],[0])
m4_define([lt_version],[1:0:0])
m4_define([package_version],[major_version.minor_version.micro_version-git])
m4_define([bug_report_url],[https://gitlab.com/mimir-project/mimir/issues])


dnl ***********************************************************************
dnl Initialize autoconf
dnl ***********************************************************************
AC_INIT([mimir],[package_version],[bug_report_url])
AC_CONFIG_HEADERS([config.h])
AC_CONFIG_SRCDIR([data/libmimir.pc.in])
AC_CONFIG_MACRO_DIR([m4])
AC_CONFIG_AUX_DIR([build-aux])
AC_CANONICAL_HOST

dnl ***********************************************************************
dnl Check if this is a release build
dnl ***********************************************************************
AX_IS_RELEASE([git-directory])

AM_CONDITIONAL([IS_RELEASE], [test x$ax_is_release = xyes])

dnl ***********************************************************************
dnl Make version information available to autoconf files
dnl ***********************************************************************
AC_SUBST([MAJOR_VERSION],major_version)
AC_SUBST([MINOR_VERSION],minor_version)
AC_SUBST([MICRO_VERSION],micro_version)
AC_SUBST([LT_VERSION], lt_version)


dnl ***********************************************************************
dnl Initialize automake
dnl ***********************************************************************
AM_SILENT_RULES([yes])
AM_INIT_AUTOMAKE([1.11 foreign subdir-objects tar-ustar dist-xz dist-bzip2 -Wall -Werror])
AX_GENERATE_CHANGELOG
AX_CODE_COVERAGE


dnl ***********************************************************************
dnl Add extra debugging with --enable-debug and --enable-compile-warnings
dnl ***********************************************************************
AX_CHECK_ENABLE_DEBUG([yes])

dnl Check if code-coverage and debug is enabled. WITH_DEBUG can be used
dnl in Makefile.am to add conditional compiling for files which are only
dnl needen when debugging is enabled.
AS_IF([test x$enable_code_coverage = xyes], [enabledebug=no],
      [test x$ax_enable_debug = xyes], [enabledebug=yes], [enabledebug=no])
AM_CONDITIONAL([ENABLE_DEBUG], [test x$enabledebug = xyes])


dnl ***********************************************************************
dnl Documentation support
dnl ***********************************************************************
AC_ARG_ENABLE([doc],
	AS_HELP_STRING([--enable-doc],[Whether to support documentation generation]),
	enable_doc_generation=yes,
	enable_doc_generation=no)

AS_IF([ test "$enable_doc_generation" = "yes" ], [
	AC_CHECK_PROGS([DOC_DEPENDENCIES], [doxygen sphinx-build], [no])
	AS_IF([test "x$DOC_DEPENDENCIES" = xno],
		AC_MSG_WARN(doxygen and sphinx-build are required to build documentation),
		AC_CONFIG_FILES([docs/Doxyfile docs/conf.py])
		)
	])
AC_CONFIG_FILES([docs/Makefile])
AM_CONDITIONAL([GENERATE_DOC], [test x$enable_doc_generation = xyes])
AM_EXTRA_RECURSIVE_TARGETS([htmldoc])


dnl ***********************************************************************
dnl Internationalization
dnl ***********************************************************************
dnl GETTEXT_PACKAGE=AC_PACKAGE_TARNAME
dnl AC_SUBST([GETTEXT_PACKAGE])
dnl AC_DEFINE_UNQUOTED([GETTEXT_PACKAGE], ["$GETTEXT_PACKAGE"], [GETTEXT package name])

dnl AM_GNU_GETTEXT_VERSION([0.19.3])
dnl AM_GNU_GETTEXT([external])

dnl ***********************************************************************
dnl Check for required programs
dnl ***********************************************************************
AC_PROG_CC
AC_PROG_INSTALL
AC_PROG_SED
AM_PROG_AR

AX_COMPILER_FLAGS

AX_VALGRIND_DFLT([sgcheck], [off])
AX_VALGRIND_CHECK

dnl ***********************************************************************
dnl Check if compiler understands visbility flags and linker supports
dnl version scripts
dnl ***********************************************************************
gl_VISIBILITY
AM_CONDITIONAL([HAVE_VISIBILITY], [test "x$HAVE_VISIBILITY" = x1])
gl_LD_VERSION_SCRIPT


dnl ***********************************************************************
dnl Optional 'check' unit test support
dnl When not on release this option is enabled on default
dnl ***********************************************************************
AS_IF([test "x$ax_is_release" = "xno"], [with_check="yes"])

AC_ARG_WITH([check],
    AS_HELP_STRING([--with-check], [With 'check' unit test support]))

AS_IF([test "x$with_check" = "xyes"],
	[PKG_CHECK_MODULES([CHECK], [check >= 0.9.6])])

AM_CONDITIONAL([WITH_CHECK], [test x$with_check = xyes])

dnl ***********************************************************************
dnl Ensure C11 is Supported
dnl ***********************************************************************
AX_CHECK_COMPILE_FLAG([-std=gnu11],
                      [CFLAGS="$CFLAGS -std=gnu11"],
                      [AC_MSG_ERROR([C compiler cannot compile GNU C11 code])])

dnl ***********************************************************************
dnl Check for functions
dnl ***********************************************************************
AC_CHECK_FUNCS([ \
		__secure_getenv \
		secure_getenv \
	])

AC_CHECK_FUNC([clock_gettime], [AC_DEFINE([HAVE_CLOCK_GETTIME], [1],
		[Define to 1 if you have 'clock_gettime()' function.])
		have_clock_gettime=yes])
AM_CONDITIONAL([HAVE_CLOCK_GETTIME], [test x$have_clock_gettime = xyes])

dnl ***********************************************************************
dnl Check for required packages
dnl ***********************************************************************
PKG_CHECK_MODULES([CRYPTO], [libcrypto >= 1.1.0])


dnl ***********************************************************************
dnl Initialize Libtool
dnl ***********************************************************************
LT_PREREQ([2.2])
LT_INIT


dnl ***********************************************************************
dnl Process .in Files
dnl ***********************************************************************
AC_CONFIG_FILES([
	Makefile

	lib/Makefile
	lib/include/Makefile
	lib/include/libmimir-version.h

	tests/Makefile

	src/Makefile

	data/Makefile
	data/libmimir.pc:data/libmimir.pc.in

],[],
[API_VERSION='$API_VERSION'])
AC_OUTPUT

echo "
${PACKAGE} - ${VERSION}

Prefix:		${prefix}
Libdir:		${libdir}
Compiler:	${CC}
Compiler Flags:	${CFLAGS} ${CPPFLAGS} ${LDFLAGS} ${WARN_CFLAGS} ${WARN_LDFLAGS}
Release:	${ax_is_release}

Type 'make' to build ${PACKAGE}
"
