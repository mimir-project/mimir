/* card.c
 *
 * Copyright (C) 2017 Jens Sauer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <libmimir.h>
#include "libmimir-private.h"

/**
 * Mask for ``color`` property
 */
#define MIMIR_CARD_COLOR_MASK 		0xF00
/**
 * Mask for ``form`` property
 */
#define MIMIR_CARD_FORM_MASK		0x0F0
/**
 * Mask for ``number`` property
 */
#define MIMIR_CARD_NUMBER_MASK		0x00F

static void destroy_card(struct mimir_card *c);
static int properties_valid(const unsigned int properties);
static char *make_symbol(const unsigned int color, const unsigned int form);
static char *add_symbol(char *dest, const char *card_template,
			const char *symbol, const int nsym);
static char *card_to_text(const unsigned int number, const unsigned int color,
			  const unsigned int form);

/**
 * Create new mimir_card.
 *
 * Each card has three #mimir_card_property values.
 * A card can only have one form, color or number at the same time.
 *
 * The private reference count is set to ``1`` after creation.
 * Must be unreferenced with card_unref_private().
 *
 * @param card Return location for a new mimir_card
 * @param properties Card properties
 * @return ``0`` on success, ``-MIMIR_ERR_INVAL`` when properties are
 * invalid or ``-MIMIR_ERR_NOMEM`` when no memory was available
 */
int card_new(struct mimir_card **card, const unsigned int properties)
{
	if (properties_valid(properties) == 0)
		return -MIMIR_ERR_INVAL;

	*card = malloc(sizeof(struct mimir_card));
	if (*card == NULL)
		return -MIMIR_ERR_NOMEM;

	(*card)->property_flags = properties;
	(*card)->public_ref_count = 0;
	(*card)->private_ref_count = 1;

	return 0;
}

/*
 * Evaluate if properties is a valid combination of mimir_card_property
 * The following cases lead to a 'invalid' state of properties:
 *
 *  - No flags are passed
 *
 *  - More than 12 bits are set. The property flags are in the lower 12 bit.
 *
 *  - Each category has exactly one bit. Not zero, not two or more.
 *
 * Returns a succeeded boolean: 0 on failure, 1 on success
 */
static int properties_valid(const unsigned int properties)
{
	unsigned int color = properties & MIMIR_CARD_COLOR_MASK;
	unsigned int form = properties & MIMIR_CARD_FORM_MASK;
	unsigned int number = properties & MIMIR_CARD_NUMBER_MASK;

	if (properties == 0)
		return 0;
	if (properties >> 12)
		return 0;
	if (color == 0)
		return 0;
	if (color & (color - 1))
		return 0;
	if (form == 0)
		return 0;
	if (form & (form - 1))
		return 0;
	if (number == 0)
		return 0;
	if (number & (number - 1))
		return 0;

	return 1;
}

/*
 * Destroy c when both ref counts are zero
 */
static void destroy_card(struct mimir_card *c)
{
	if (c->public_ref_count == 0 && c->private_ref_count == 0)
		free(c);
}

/**
 * Decreases the private reference count of card.
 *
 * When its reference count drops to zero, the card is destroyed.
 * If the pointer to the mimir_card may be reused in the future, it is
 * recommended to clear the pointer to ``NULL`` to avoid a pointer to a
 * potentially invalid mimir_card.
 *
 * @param card A mimir_card
 */
void card_unref_private(struct mimir_card *card)
{
	if (card == NULL)
		return;

	if (card->private_ref_count > 0) {
		card->private_ref_count--;
		destroy_card(card);
	}
}

/**
 * Increase the private reference count of card.
 *
 * Decrease private reference count with card_unref_private()
 *
 * @param card A mimir_card
 * @return The same mimir_card
 */
struct mimir_card *card_ref_private(struct mimir_card *card)
{
	if (card == NULL)
		return NULL;

	card->private_ref_count++;

	return card;
}

/**
 * Increases the reference count of card.
 *
 * Decrease reference count with mimir_card_unref()
 *
 * @param card A mimir_card
 * @return The same mimir_card
 */
struct mimir_card *mimir_card_ref(struct mimir_card *card)
{
	if (card == NULL)
		return NULL;

	card->public_ref_count++;

	return card;
}

/**
 * Decreases the reference count of card.
 *
 * When its reference count drops to zero, the card is destroyed.
 * If the pointer to the mimir_card may be reused in the future, it is
 * recommended to clear the pointer to ``NULL`` to avoid a pointer to a
 * potentially invalid mimir_card.
 *
 * @param card A mimir_card
 */
void mimir_card_unref(struct mimir_card *card)
{
	if (card == NULL)
		return;

	if (card->public_ref_count > 0) {
		card->public_ref_count--;
		destroy_card(card);
	}
}

/**
 * Fetches the form of card.
 *
 * @param card A mimir_card
 * @return #mimir_card_property or -1 on failure.
 */
int mimir_card_get_form(struct mimir_card *card)
{
	if (card == NULL)
		return -1;

	return card->property_flags & MIMIR_CARD_FORM_MASK;
}

/**
 * Fetches the color of card.
 *
 * @param card A mimir_card
 * @return #mimir_card_property or -1 on failure.
 */
int mimir_card_get_color(struct mimir_card *card)
{
	if (card == NULL)
		return -1;

	return card->property_flags & MIMIR_CARD_COLOR_MASK;
}

/**
 * Fetches the number of card
 *
 * @param card A mimir_card
 * @return A #mimir_card_property or -1 on failure.
 */
int mimir_card_get_number(struct mimir_card *card)
{
	if (card == NULL)
		return -1;

	return card->property_flags & MIMIR_CARD_NUMBER_MASK;
}

/**
 * Fetches all card properties.
 *
 * @param card A mimir_card
 * @param form Address to store the form
 * @param color Address to store the color
 * @param number Address to store the number
 * @return 0 on success, -1 on failure
 */
int mimir_card_get_props(struct mimir_card *card, unsigned int *form,
			 unsigned int *color, unsigned int *number)
{
	if (card == NULL || form == NULL || color == NULL || number == NULL)
		return -1;

	*form = card->property_flags & MIMIR_CARD_FORM_MASK;
	*color = card->property_flags & MIMIR_CARD_COLOR_MASK;
	*number = card->property_flags & MIMIR_CARD_NUMBER_MASK;

	return 0;
}

/**
 * Check if two mimir_card objects have equal properties.
 *
 * @param card1 First mimir_card to compare
 * @param card2 Second mimir_card to compare
 * @return 1 when one or more equal properties found, otherwise 0
 */
int mimir_card_have_equal_prop(struct mimir_card *card1,
			       struct mimir_card *card2)
{
	if (card1 == NULL || card2 == NULL)
		return 0;

	if (card1->property_flags & card2->property_flags)
		return 1;
	else
		return 0;
}

/**
 * Convert property into string.
 *
 * This will convert the given #mimir_card_property into a readable string.
 *
 * @param property The #mimir_card_property to convert
 * @return Readable string of property
 */
const char *mimir_card_prop_str(const enum mimir_card_property property)
{
	switch (property) {
	case MIMIR_CARD_PROP_ONE:
		return "One";
	case MIMIR_CARD_PROP_TWO:
		return "Two";
	case MIMIR_CARD_PROP_THREE:
		return "Three";
	case MIMIR_CARD_PROP_FOUR:
		return "Four";
	case MIMIR_CARD_PROP_TRIANGLE:
		return "Triangle";
	case MIMIR_CARD_PROP_STAR:
		return "Star";
	case MIMIR_CARD_PROP_CROSS:
		return "Cross";
	case MIMIR_CARD_PROP_CIRCLE:
		return "Circle";
	case MIMIR_CARD_PROP_RED:
		return "Red";
	case MIMIR_CARD_PROP_GREEN:
		return "Green";
	case MIMIR_CARD_PROP_YELLOW:
		return "Yellow";
	case MIMIR_CARD_PROP_BLUE:
		return "Blue";
	default:
		return "Unknown property";
	}
}

/**
 * Fetch raw value from card properties
 *
 * Fetch all properties in one unsigned int.
 *
 * @param card A mimir_card
 * @return Raw value of card properties
 */
unsigned int mimir_card_get_prop_raw(struct mimir_card *card)
{
	if (card == NULL)
		return 0;
	return card->property_flags;
}

/**
 * Extract color from raw_properties
 *
 * The color of a raw value from mimir_card_get_prop_raw() can be seperated
 * with this function.
 *
 * @param raw_properties Raw properties value of a #mimir_card
 * @return #mimir_card_property representing the color
 */
unsigned int mimir_card_raw_to_color(const unsigned int raw_properties)
{
	if (properties_valid(raw_properties))
		return raw_properties & MIMIR_CARD_COLOR_MASK;
	else
		return 0;
}

/**
 * Extract number from raw_properties
 *
 * The number of a raw value from mimir_card_get_prop_raw() can be seperated
 * with this function.
 *
 * @param raw_properties Raw properties value of a #mimir_card
 * @return #mimir_card_property representing the number
 */
unsigned int mimir_card_raw_to_number(const unsigned int raw_properties)
{
	if (properties_valid(raw_properties))
		return raw_properties & MIMIR_CARD_NUMBER_MASK;
	else
		return 0;
}

/**
 * Extract form from raw_properties
 *
 * The form of a raw value from mimir_card_get_prop_raw() can be seperated
 * with this function.
 *
 * @param raw_properties Raw properties value of a #mimir_card
 * @return #mimir_card_property representing the form
 */
unsigned int mimir_card_raw_to_form(const unsigned int raw_properties)
{
	if (properties_valid(raw_properties))
		return raw_properties & MIMIR_CARD_FORM_MASK;
	else
		return 0;
}

/**
 * Fetch text version of card
 *
 * This composes a terminal colored string of the card.
 *
 * @param card #mimir_card to compose ascii
 * @param dest Adress to store string
 * @return ``0`` on success, ``-#MIMIR_ERR_INVAL`` on incorrect parameter
 * or ``-#MIMIR_ERR_NOMEM`` when no memory is available
 */
int mimir_card_get_text(struct mimir_card *card, char **dest)
{
	unsigned int number;
	unsigned int color;
	unsigned int form;
	char *card_text;

	if (!card)
		return -MIMIR_ERR_INVAL;
	if (!dest)
		return -MIMIR_ERR_INVAL;
	mimir_card_get_props(card, &form, &color, &number);
	card_text = card_to_text(number, color, form);
	if (!card_text)
		return -MIMIR_ERR_NOMEM;
	*dest = card_text;

	return 0;
}

/**
 * Add left padding to card text string
 *
 * For easier alignement, this function adds spaces as padding at the
 * left side of the card. Works with strings fom mimir_card_get_text() and
 * mimir_assessment_get_ref_cards_text().
 *
 * @param text_card A card text string
 * @param padding Number of spaces to add
 */
char *mimir_card_text_add_padding(char *text_card, const size_t padding)
{
	char *card;
	char *from;
	char *to;
	char *tmp;
	size_t remain_len;

	if (!text_card)
		return NULL;
	if (!padding)
		return text_card;
	card = text_card;
	card = realloc(text_card, (strlen(card) + 7 * padding + 1) *
		       sizeof(char));
	if (!card)
		return text_card;
	from = card;
	do {
		remain_len = strlen(from) + 1;
		to = from + padding;
		memmove(to, from, remain_len);
		memset(from, ' ', padding);
		from = memchr(to, '\n', remain_len);
		tmp = from;
		from++;
	} while (tmp != NULL);

	return card;
}

/*
 * Return a string containing a mimir_card as text with the given properties.
 */
static char *card_to_text(const unsigned int number, const unsigned int color,
			  const unsigned int form)
{
	const char *template =
	    "╭───────╮\n"
	    "│       │\n"
	    "│       │\n"
	    "│       │\n"
	    "│       │\n"
	    "│       │\n"
	    "╰───────╯";
	char *card;
	char *symbol;
	size_t template_len = strlen(template);
	size_t symbol_len;
	int number_value;

	switch (number) {
	case MIMIR_CARD_PROP_ONE:
		number_value = 1;
		break;
	case MIMIR_CARD_PROP_TWO:
		number_value = 2;
		break;
	case MIMIR_CARD_PROP_THREE:
		number_value = 3;
		break;
	case MIMIR_CARD_PROP_FOUR:
		number_value = 4;
		break;
	default:
		number_value = 0;
		break;
	}
	symbol = make_symbol(color, form);
	if (!symbol)
		goto out;
	symbol_len = strlen(symbol);
	card = malloc((template_len + symbol_len * number_value + 1) *
		      sizeof(char));
	if (!card)
		goto out_free_sym;
	card = add_symbol(card, template, symbol, number_value);
	free(symbol);
	return card;

 out_free_sym:
	free(symbol);
 out:
	return NULL;
}

/*
 * Copy card_template and insert symbol nsym times at defined positions.
 */
static char *add_symbol(char *dest, const char *card_template,
			const char *symbol, const int nsym)
{
	size_t write_offset = 0;
	size_t read_offset = 0;
	size_t bytes;
	size_t template_len = strlen(card_template);
	size_t symbol_len = strlen(symbol);
	int count = 0;

	/*
	 * Set bytes to read until first symbol position
	 */
	if (nsym == 1)
		bytes = 62;
	else if (nsym == 2 || nsym == 3)
		bytes = 48;
	else
		bytes = 47;

	while (read_offset < template_len) {
		memcpy(dest + write_offset, card_template + read_offset,
		       sizeof(char) * bytes);
		write_offset += bytes;
		read_offset += bytes + 1;
		if (count < nsym) {
			memcpy(dest + write_offset, symbol,
			       sizeof(char) * symbol_len);
			write_offset += symbol_len;
			count++;
		}
		/*
		 * Set bytes until next symbol position
		 */
		if (nsym == count)
			bytes = template_len + 1 - read_offset;
		if (nsym == 2 && count == 1)
			bytes = 76 - read_offset;
		if ((nsym == 3 && count == 1) || (nsym == 4 && count == 2))
			bytes = 75 - read_offset;
		if ((nsym == 3 && count == 2) || (nsym == 4 && count == 3))
			bytes = 77 - read_offset;
		if (nsym == 4 && count == 1)
			bytes = 49 - read_offset;
	}
	return dest;
}

/*
 * Compose a string containing a unicode symbol surrounded
 * by ANSI terminal color escape code. The returned string must be freed.
 * The parameters color and form are values of enum mimir_card_property.
 */
static char *make_symbol(const unsigned int color, const unsigned int form)
{
	char *symbol;
	int size = 0;
	const char *form_value;
	int color_value;

	switch (form) {
	case MIMIR_CARD_PROP_CIRCLE:
		form_value = "●";
		break;
	case MIMIR_CARD_PROP_TRIANGLE:
		form_value = "▲";
		break;
	case MIMIR_CARD_PROP_CROSS:
		form_value = "✖";
		break;
	case MIMIR_CARD_PROP_STAR:
		form_value = "★";
		break;
	default:
		form_value = "A";
		break;
	};
	switch (color) {
	case MIMIR_CARD_PROP_RED:
		color_value = 31;
		break;
	case MIMIR_CARD_PROP_BLUE:
		color_value = 34;
		break;
	case MIMIR_CARD_PROP_YELLOW:
		color_value = 33;
		break;
	case MIMIR_CARD_PROP_GREEN:
		color_value = 32;
		break;
	default:
		color_value = 0;
		break;
	};
	symbol = malloc(sizeof(char) * 13);
	if (!symbol)
		return NULL;
	size = snprintf(symbol, 13, "\x1b[%dm%s\x1b[0m", color_value,
			form_value);
	if (size < 0) {
		free(symbol);
		return NULL;
	}

	return symbol;
}
