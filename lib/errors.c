/* errors.c
 *
 * Copyright (C) 2017 Jens Sauer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libmimir.h>
#include "libmimir-private.h"

/**
 * Translate MimirErrorCode into human friendly string.
 *
 * Accepts positive and negative ``MIMIR_ERR_XXX`` values
 *
 * @param error Error return value from a libmimir function
 *
 * @return A const char holding the error message.
 */
const char *mimir_err_get_message(int error)
{
	if (error < 0)
		error = -error;

	switch (error) {
	case MIMIR_ERR_NOMEM:
		return "No memory available";
	case MIMIR_ERR_ABORT:
		return "Can not continue! Abort!";
	case MIMIR_ERR_INVAL:
		return "Invalid argument";
	case MIMIR_ERR_NORSP:
		return "No response for last card";
	case MIMIR_ERR_AFIN:
		return "Assessment is finished";
	case MIMIR_ERR_IO:
		return "Input/Output error";
	default:
		return "Unknown error";
	}
}
