/* libmimir.h
 *
 * Copyright (C) 2017 Jens Sauer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBMIMIR_H
#define LIBMIMIR_H

#ifdef  __cplusplus
#define LIBMIMIR_BEGIN_DECLS  extern "C" {
#define LIBMIMIR_END_DECLS    }
#else
#define LIBMIMIR_BEGIN_DECLS
#define LIBMIMIR_END_DECLS
#endif

#if defined LIBMIMIR_COMPILATION && defined HAVE_VISIBILITY
#define LIBMIMIR_EXPORT __attribute__((__visibility__("default")))
#else
#define LIBMIMIR_EXPORT
#endif

#define LIBMIMIR_INSIDE
#include <libmimir-version.h>
#include <libmimir-time.h>
#undef LIBMIMIR_INSIDE

#define MIMIR_N_CARDS		64	/* cards per stack */

/*
 * Error codes
 */
#define MIMIR_ERR_NOMEM		1	/**< No memory available */
#define MIMIR_ERR_ABORT		2	/**< Test can not be continued */
#define MIMIR_ERR_INVAL		3	/**< Invalid argument */
#define MIMIR_ERR_NOCARD	4	/**< No card to compare */
#define MIMIR_ERR_BADRSP	5	/**< Response was incorrect */
#define MIMIR_ERR_NORSP		6	/**< No response for last card*/
#define MIMIR_ERR_AFIN		7	/**< Assessment is finished */
#define MIMIR_ERR_IO		8	/**< Input/output error */

#define MIMIR_FILE_VERSION	1	/**< current file format version */

LIBMIMIR_BEGIN_DECLS
/*
 * Error handling
 */
LIBMIMIR_EXPORT const char *mimir_err_get_message(int error);
LIBMIMIR_EXPORT int mimir_has_debug(void);

struct mimir_card;

/**
 * Card properties
 *
 * Each card in libmimir has three properties, one from each category.
 */
enum mimir_card_property {
	MIMIR_CARD_PROP_ONE = 1 << 0,		/**< One form */
	MIMIR_CARD_PROP_TWO = 1 << 1,		/**< Two forms */
	MIMIR_CARD_PROP_THREE = 1 << 2,		/**< Three forms  */
	MIMIR_CARD_PROP_FOUR = 1 << 3,		/**< Four forms */
	MIMIR_CARD_PROP_TRIANGLE = 1 << 4,	/**< Triangle as form */
	MIMIR_CARD_PROP_STAR = 1 << 5,		/**< Star as form */
	MIMIR_CARD_PROP_CROSS = 1 << 6,		/**< Cross as form */
	MIMIR_CARD_PROP_CIRCLE = 1 << 7,	/**< Circle as form */
	MIMIR_CARD_PROP_RED = 1 << 8,		/**< Red colored form*/
	MIMIR_CARD_PROP_GREEN = 1 << 9,		/**< Green colored form */
	MIMIR_CARD_PROP_YELLOW = 1 << 10,	/**< Yellow colored form */
	MIMIR_CARD_PROP_BLUE = 1 << 11,		/**< Blue colored form */
};

LIBMIMIR_EXPORT int mimir_card_get_form(struct mimir_card *card);
LIBMIMIR_EXPORT int mimir_card_get_color(struct mimir_card *card);
LIBMIMIR_EXPORT int mimir_card_get_number(struct mimir_card *card);
LIBMIMIR_EXPORT int mimir_card_get_props(struct mimir_card *card,
					 unsigned int *form,
					 unsigned int *color,
					 unsigned int *number);
LIBMIMIR_EXPORT int mimir_card_have_equal_prop(struct mimir_card *card1,
					       struct mimir_card *card2);
LIBMIMIR_EXPORT const char *mimir_card_prop_str(const enum mimir_card_property
						property);
LIBMIMIR_EXPORT struct mimir_card *mimir_card_ref(struct mimir_card *card);
LIBMIMIR_EXPORT void mimir_card_unref(struct mimir_card *card);
LIBMIMIR_EXPORT unsigned int mimir_card_get_prop_raw(struct mimir_card *card);
LIBMIMIR_EXPORT unsigned int mimir_card_raw_to_color(const unsigned int
						     raw_properties);
LIBMIMIR_EXPORT unsigned int mimir_card_raw_to_number(const unsigned int
						      raw_properties);
LIBMIMIR_EXPORT unsigned int mimir_card_raw_to_form(const unsigned int
						    raw_properties);
LIBMIMIR_EXPORT int mimir_card_get_text(struct mimir_card *card, char **dest);
LIBMIMIR_EXPORT char *mimir_card_text_add_padding(char *ascii_card,
						  const size_t padding);

/**
 * Reference cards
 *
 * Each assessment has four reference cards, this enum is used as index.
 */
enum mimir_ref_card {
	MIMIR_ASMT_REF_CARD_ONE,
	MIMIR_ASMT_REF_CARD_TWO,
	MIMIR_ASMT_REF_CARD_THREE,
	MIMIR_ASMT_REF_CARD_FOUR,
	MIMIR_ASMT_REF_CARD_N
};

struct mimir;
LIBMIMIR_EXPORT int mimir_new(struct mimir **asmt);
LIBMIMIR_EXPORT void mimir_unref(struct mimir *asmt);
LIBMIMIR_EXPORT struct mimir *mimir_ref(struct mimir *asmt);
LIBMIMIR_EXPORT int mimir_get_next_card(struct mimir *asmt,
					struct mimir_card **next_card);
LIBMIMIR_EXPORT int mimir_add_response(struct mimir *asmt,
				       const enum mimir_ref_card ref_card,
				       const int time_msec);
LIBMIMIR_EXPORT struct mimir_card **mimir_get_ref_cards(struct mimir *asmt);
LIBMIMIR_EXPORT struct mimir_card *mimir_get_ref_card(struct mimir *asmt,
						      const enum mimir_ref_card
						      ref_card);
LIBMIMIR_EXPORT int mimir_get_ref_cards_text(struct mimir *asmt, char **dest);
LIBMIMIR_EXPORT int mimir_save(const struct mimir *asmt, const char *filename,
			       const char *password,
			       const unsigned int password_iteration);
LIBMIMIR_EXPORT int mimir_load(const char *filename, const char *password,
			       struct mimir **asmt);
LIBMIMIR_EXPORT time_t mimir_get_time(const struct mimir *asmt);
LIBMIMIR_EXPORT int mimir_set_pid(struct mimir *asmt,
				  const char *participant_id);
LIBMIMIR_EXPORT int mimir_get_pid(const struct mimir *asmt, char *buf,
				  const size_t buf_len);
LIBMIMIR_EXPORT int mimir_get_uuid(const struct mimir *asmt, char *buf,
				   const size_t buf_len);
LIBMIMIR_EXPORT int mimir_get_participant_uuid(const struct mimir *asmt,
					       char *buf, const size_t buf_len);

LIBMIMIR_END_DECLS
#endif				/* LIBMIMIR_H */
