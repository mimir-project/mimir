/* libmimir-time.h
 *
 * Prototypes and types to track time.
 *
 * Copyright (C) 2019 Jens Sauer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBMIMIR_TIME_H
#define LIBMIMIR_TIME_H

#if !defined(LIBMIMIR_INSIDE) && !defined(LIBMIMIR_COMPILATION)
#error "Only <libmimir.h> can be included directly."
#endif

#include <time.h>

LIBMIMIR_BEGIN_DECLS

/**
 * Struct used to track time between two points
 *
 * Uses C11 struct timespec which has a resolution of nanoseconds.
 */
struct mimir_time {
	struct timespec start;	/**< Time at first call */
	struct timespec end;	/**< Time at second call */
};

/**
 * Set first timestamp
 *
 * Save current timestamp into mimir_time.start as the first timestamp.
 * mimir_time_start() sets content of mt to zero before saveing the
 * current time.
 *
 * @param mt Pointer to mimir_time to save the time
 * @return ``0`` on success, ``-#MIMIR_ERR_INVAL`` when mt is ``NULL``,
 *	   ``1`` when other errors occur. Check ``errno`` for more
 *	   information.
 */
LIBMIMIR_EXPORT int mimir_time_start(struct mimir_time *mt);

/**
 * Set second timestamp
 *
 * Save current timestamp into mimir_time.end as the second timestamp.
 *
 * @param mt Pointer to mimir_time to save the time
 * @return ``0`` on success, ``-#MIMIR_ERR_INVAL`` when mt is ``NULL``,
 *	   ``1`` when other errors occur. Check ``errno`` for more
 *	   information.
 */
LIBMIMIR_EXPORT int mimir_time_end(struct mimir_time *mt);

/**
 * Get time difference
 *
 * Get time difference of both timestamps saved in mt in milliseconds.
 * The difference is rounded to the next integer.
 *
 * @param mt mimir_time to calculate the time differece from
 * @return time difference
 */
LIBMIMIR_EXPORT int mimir_time_diff(const struct mimir_time mt);

LIBMIMIR_END_DECLS

#endif				/* LIBMIMIR_TIME_H */
