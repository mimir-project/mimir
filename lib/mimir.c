/* mimir.c
 *
 * Copyright (C) 2017 Jens Sauer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <errno.h>
#include <libmimir.h>
#include "libmimir-private.h"

static int create_ref_cards(struct mimir_card **c);
static void destroy_ref_cards(struct mimir_card **c);
static int response_match_sort(struct mimir *asmt, struct mimir_card *ref,
			       struct mimir_card *answer);
static void switch_sort_principle(struct mimir *asmt);
static int asmt_reaches_end(struct mimir *asmt);

static struct mimir_header make_header(const unsigned int password_iterations,
				       const struct mimir_uuid asmt_uuid,
				       const struct mimir_uuid
				       participant_uuid);

static inline unsigned int pwd_iter_from_header(const struct mimir_header
						header);

static uint8_t mimir_magic_number[] = { 0x89, 0x4d, 0x49, 0x4d, 0x0d,
	0x0a, 0x1a, 0x0a
};

/**
 * Creates a new mimir object.
 *
 * When the mimir object is not longer needed it must be
 * destroyed with mimir_unref().
 *
 * @param asmt Return location for new mimir
 * @return ``0`` on success or ``-MIMIR_ERR_XXX`` failure
 */
int mimir_new(struct mimir **asmt)
{
	int ret;
	struct mimir *m;

	if (asmt == NULL)
		goto err_inval;

	m = malloc(sizeof(struct mimir));
	if (m == NULL)
		goto err_nomem;
	m->cards_used = 0;
	m->con_correct = 0;
	m->current_principle = SORT_COLOR;
	m->sequences_passed = 0;
	m->last_card = NULL;
	m->cards = NULL;
	m->ref_cards = NULL;
	m->ref_count = 1;
	m->response_list = NULL;
	m->time = time(NULL);
	m->participant_id = NULL;
	m->finished = 0;
	memset(&m->participant_uuid, 0, sizeof(struct mimir_uuid));

	debug("create assessment %p\n", m);
	debug("create stimulus cards for assessment %p\n", m);

	ret = card_list_new(&(m->cards));
	if (ret)
		goto err;
	m->ref_cards = calloc(sizeof(struct mimir_card *),
			      MIMIR_ASMT_REF_CARD_N);
	if (m->ref_cards == NULL)
		goto err_nomem_free;
	debug("create reference cards for assessment %p\n", m);
	ret = create_ref_cards(m->ref_cards);
	if (ret)
		goto err;

	ret = response_list_new(&(m->response_list), m);
	if (ret)
		goto err;

	ret = uuid_v4(&m->asmt_uuid);
	if (ret)
		goto err;

	*asmt = m;

	return 0;

 err_inval:
	debug("invalid arguments passed\n");
	return -MIMIR_ERR_INVAL;
 err_nomem_free:
	mimir_unref(m);
 err_nomem:
	debug("out of memory, malloc() failed\n");
	return -MIMIR_ERR_NOMEM;
 err:
	mimir_unref(m);
	return ret;
}

/**
 * Fetches the next card which should be displayed to the user from asmt.
 *
 * Call mimir_card_unref() on 'next_card', when no longer used.
 *
 * @param asmt A mimir object
 * @param next_card Pointer to store the next card
 * @return ``0`` on success. ``-#MIMIR_ERR_NORSP`` when there is no response
 * for the last card you fetched. ``-#MIMIR_ERR_AFIN`` when the assessment
 * is finished. ``-#MIMIR_ERR_INVAL`` when ``asmt`` or ``next_card`` are
 * invalid arguments.
 */
int mimir_get_next_card(struct mimir *asmt, struct mimir_card **next_card)
{
	struct mimir_card *card;
	int end;

	if (asmt == NULL)
		goto err_inval_asmt;
	if (!next_card)
		goto err_inval;
	if (asmt->last_card)
		goto err_no_rsp;

	end = asmt_reaches_end(asmt);
	if (end) {
		debug("assessment '%p' is finished\n", asmt);
		return -MIMIR_ERR_AFIN;
	} else {
		card = card_list_get_next(asmt->cards);
		asmt->cards_used++;
		asmt->last_card = mimir_card_ref(card);
		*next_card = card;
		debug("fetched card '%d' for assessment '%p'\n",
		      asmt->cards_used, asmt);
		return 0;
	}

 err_inval:
	debug("next_card is NULL pointer\n");
 err_inval_asmt:
	return -MIMIR_ERR_INVAL;
 err_no_rsp:
	debug("no response for last card, can not return next card\n");
	return -MIMIR_ERR_NORSP;
}

/**
 * Decreases the reference count of asmt.
 *
 * When its reference count drops to zero, the asmt is destroyed.
 * If the pointer to mimir may be reused in the future,
 * it is recommended to clear the pointer to ``NULL`` to avoid a pointer
 * to a potentially invalid mimir.
 *
 * @param asmt The mimir object to unref
 */
void mimir_unref(struct mimir *asmt)
{
	if (asmt == NULL)
		return;

	asmt->ref_count--;
	debug("decrease refererence count for assessment %p\n", asmt);

	if (asmt->ref_count == 0) {
		debug("release assessment %p\n", asmt);
		card_list_unref(asmt->cards);
		destroy_ref_cards(asmt->ref_cards);
		response_list_free(asmt->response_list);
		free(asmt->ref_cards);
		free(asmt->participant_id);
		free(asmt);
	}
}

/**
 * Increases the reference count of asmt.
 *
 * @param asmt A mimir
 * @return The same mimir
 */
struct mimir *mimir_ref(struct mimir *asmt)
{
	if (asmt == NULL)
		return NULL;

	asmt->ref_count++;
	debug("increase reference count for assessment %p\n", asmt);
	return asmt;
}

/**
 * Report participants response to asmt.
 *
 * Tell asmt which reference card the participant used to sort the last card.
 *
 * @param asmt A #mimir object
 * @param ref_card The #mimir_ref_card the participant used
 * @param time_msec Time period the participant took to complete the stimulus
 *	in milliseconds. Any negative value disables this feature
 * @return ``0`` when response was correct, ``-#MIMIR_ERR_BADRSP`` when
 * incorrect. ``-#MIMIR_ERR_INVAL`` when ``asmt`` or ``ref_card`` are invalid.
 */
int mimir_add_response(struct mimir *asmt, const enum mimir_ref_card ref_card,
		       const int time_msec)
{
	struct mimir_card *reference_card;
	struct mimir_card *last_card;
	int correct;
	int err;

	if (!asmt)
		goto err_no_asmt;
	reference_card = mimir_get_ref_card(asmt, ref_card);
	if (!reference_card)
		goto err_ref;
	last_card = asmt->last_card;
	if (!last_card)
		goto err_no_last_card;

	debug("received response with reference card '%d'\n", ref_card);
	debug("reference card %#x, stimulus card %#x\n",
	      mimir_card_get_prop_raw(reference_card),
	      mimir_card_get_prop_raw(last_card));

	switch_sort_principle(asmt);
	correct = response_match_sort(asmt, reference_card, last_card);
	debug("response was %s\n", correct ? "correct" : "incorrect");
	if (correct)
		asmt->con_correct++;
	else
		asmt->con_correct = 0;
	debug("con_correct: %d\n", asmt->con_correct);

	err = response_list_append(asmt->response_list, reference_card,
				   last_card, asmt->current_principle, correct,
				   time_msec);
	if (err) {
		mimir_card_unref(reference_card);
		mimir_card_unref(last_card);
		goto out;
	}

	mimir_card_unref(reference_card);
	mimir_card_unref(last_card);
	asmt->last_card = NULL;

	return correct ? 0 : -MIMIR_ERR_BADRSP;

 err_no_asmt:
	return -MIMIR_ERR_INVAL;
 err_ref:
	debug("reference card '%d' is invalid\n", ref_card);
	return -MIMIR_ERR_INVAL;
 err_no_last_card:
	mimir_card_unref(reference_card);
	debug("no card to compare. " "Did you call mimir_get_next_card()?\n");
	return -MIMIR_ERR_NOCARD;
 out:
	return err;
}

/**
 * Fetches all reference cards which are used by asmt.
 *
 * Call mimir_card_unref() on all reference cards in the array when
 * they are no longer used.
 *
 * @param asmt A mimir object
 * @return An array of mimir_card object with length of MIMIR_ASMT_REF_CARD_N
 */
struct mimir_card **mimir_get_ref_cards(struct mimir *asmt)
{
	if (asmt == NULL)
		return NULL;

	debug("fetch all reference cards\n");
	for (int i = 0; i < MIMIR_ASMT_REF_CARD_N; i++)
		mimir_card_ref(asmt->ref_cards[i]);

	return asmt->ref_cards;
}

/**
 * Fetches a specific reference card which is used by asmt.
 *
 * Call mimir_card_unref() when the reference card is no longer used.
 *
 * @param asmt A mimir object
 * @param ref_card The #mimir_ref_card to return
 * @return A mimir_card
 */
struct mimir_card *mimir_get_ref_card(struct mimir *asmt,
				      const enum mimir_ref_card ref_card)
{
	if (asmt == NULL)
		return NULL;

	debug("fetch reference card %d\n", ref_card);
	if (ref_card < MIMIR_ASMT_REF_CARD_N)
		return mimir_card_ref(asmt->ref_cards[ref_card]);
	else
		return NULL;
}

/**
 * Fetch reference cards as string.
 *
 * The string contains the reference cards formatted with with unicode
 * characters and ANSI terminal colors. Free the string, when no longer used.
 *
 * @param asmt A #mimir
 * @param dest Adress where to store the string.
 * @return ``0`` on success, ``-#MIMIR_ERR_INVAL`` when arguments are invalid,
 * ``-#MIMIR_ERR_NOMEM`` when no memory was available.
 */
int mimir_get_ref_cards_text(struct mimir *asmt, char **dest)
{
	struct mimir_card **ref_cards;
	char *text_cards[MIMIR_ASMT_REF_CARD_N] = { 0 };
	char *complete;
	char *save[MIMIR_ASMT_REF_CARD_N];
	char *token[MIMIR_ASMT_REF_CARD_N];
	size_t len = 0;
	int err;

	if (!asmt)
		return -MIMIR_ERR_INVAL;
	if (!dest) {
		debug("invalid destination for reference card string\n");
		return -MIMIR_ERR_INVAL;
	}
	debug("fetch reference cards as string\n");
	ref_cards = mimir_get_ref_cards(asmt);

	for (int i = 0; i < MIMIR_ASMT_REF_CARD_N; i++) {
		err = mimir_card_get_text(ref_cards[i], &text_cards[i]);
		if (err)
			goto err;
		len += strlen(text_cards[i]);
	}
	complete = malloc((len + 151) * sizeof(char));
	if (!complete) {
		err = -MIMIR_ERR_NOMEM;
		debug("out of memory, malloc() failed\n");
		goto err;
	}
	/*
	 * Initialize first element for strcat
	 */
	memset(complete, 0, 1);
	for (int i = 0; i < MIMIR_ASMT_REF_CARD_N; i++)
		token[i] = strtok_r(text_cards[i], "\n", &save[i]);

	while (token[0] != NULL) {
		for (int i = 0; i < MIMIR_ASMT_REF_CARD_N; i++) {
			strcat(complete, token[i]);
			if (i != 3)
				strcat(complete, "        ");
			token[i] = strtok_r(NULL, "\n", &save[i]);
		}
		if (token[0] != NULL)
			strcat(complete, "\n");
	}
	*dest = complete;
	for (int i = 0; i < MIMIR_ASMT_REF_CARD_N; i++) {
		mimir_card_unref(ref_cards[i]);
		free(text_cards[i]);
	}

	return 0;

 err:
	for (int i = 0; i < MIMIR_ASMT_REF_CARD_N; i++) {
		mimir_card_unref(ref_cards[i]);
		free(text_cards[i]);
	}
	return err;
}

int mimir_save(const struct mimir *asmt, const char *filename,
	       const char *password, const unsigned int password_iterations)
{
	struct mimir_header header;
	FILE *fp;
	size_t len;
	unsigned char key[MIMIR_KEY_BYTES];
	int ret;
	unsigned char *ciphertext;
	unsigned char *plaintext;
	size_t plaintext_len;
	size_t cipertext_len;

	if (asmt == NULL)
		goto out_err_inval;
	if (filename == NULL) {
		debug("Can not save assessment: Empty filename\n");
		goto out_err_inval;
	}
	if (password == NULL) {
		debug("Can not safe assessment: Empty password\n");
		goto out_err_inval;
	}
	debug("save assessment in: %s\n", filename);
	debug("assessment: %p, password iterations: %d\n", asmt,
	      password_iterations);

	fp = fopen(filename, "wb");
	if (fp == NULL) {
		debug("Can not open file: %s\n", strerror(errno));
		ret = -MIMIR_ERR_IO;
		goto out;
	}
	header = make_header(password_iterations, asmt->asmt_uuid,
			     asmt->participant_uuid);
	len = fwrite(&header, 1, sizeof(header), fp);
	if (len < sizeof(header)) {
		debug("Can not save assessment: Writing header failed: %s",
		      strerror(errno));
		ret = -MIMIR_ERR_IO;
		goto out_close_file;
	}

	/*
	 * Derive Key for encryption
	 */
	debug("derive encryption key from password\n");
	ret = derive_key(password, strlen(password), header.password_salt,
			 sizeof(header.password_salt),
			 pwd_iter_from_header(header), key);
	if (ret) {
		debug("Can not save assessment: Error on deriving key");
		ret = 1;
		goto out_close_file;
	}

	ret = serialize_assessment(&plaintext, &plaintext_len, asmt);
	if (ret) {
		debug("Can not save assessment: serialization failed");
		goto out_close_file;
	}

	ciphertext = malloc(sizeof(unsigned char) * (plaintext_len +
						     MIMIR_TAG_BYTES));
	if (ciphertext == NULL) {
		debug("Can not save assessment: Out of memory");
		ret = -MIMIR_ERR_NOMEM;
		goto out_free_plain;
	}

	ret = encrypt((unsigned char *)plaintext, plaintext_len,
		      (unsigned char *)&header, sizeof(header),
		      key, ciphertext, &cipertext_len);
	if (ret) {
		debug("Can not save assessment: Encryption failed");
		ret = 1;
		goto out_free_cipher;
	}

	len = fwrite(ciphertext, 1, cipertext_len, fp);
	if (len < cipertext_len) {
		debug("Can not save assessment: Writing file failed %s",
		      strerror(errno));
		ret = -MIMIR_ERR_IO;
		goto out_free_cipher;
	}
	free(ciphertext);
	free(plaintext);
	fclose(fp);

	return 0;

 out_free_cipher:
	free(ciphertext);
 out_free_plain:
	free(plaintext);
 out_close_file:
	fclose(fp);
 out:
	return ret;
 out_err_inval:
	return -MIMIR_ERR_INVAL;
}

int mimir_load(const char *filename, const char *password, struct mimir **asmt)
{
	FILE *fp;
	struct mimir_header header;
	size_t len;
	int err;
	unsigned char key[MIMIR_KEY_BYTES];
	unsigned char buf[4096];
	unsigned char *ciphertext;
	unsigned char *plaintext;
	unsigned char *tmp;
	size_t buf_len;
	size_t cipertext_len;
	size_t plaintext_len;

	if (filename == NULL) {
		debug("Can not load assessment: Empty filename\n");
		goto out_err_inval;
	}
	if (password == NULL) {
		debug("Can not load assessment: Empty password\n");
		goto out_err_inval;
	}
	if (asmt == NULL) {
		debug("Can not load assessment: Empty pointer to assessment\n");
		goto out_err_inval;
	}
	debug("Load mimir file: %s\n", filename);

	fp = fopen(filename, "rb");
	if (fp == NULL) {
		debug("Can not load assessment: %s\n", strerror(errno));
		err = -MIMIR_ERR_IO;
		goto out;
	}

	len = fread(&header, 1, sizeof(header), fp);
	if (len < sizeof(header)) {
		debug("Can not load assessment: Error reading header %s\n",
		      strerror(errno));
		err = -MIMIR_ERR_IO;
		goto out_close_file;
	}
	/*
	 * check if file is a saved mimir file
	 */
	err = memcmp(&(header.magic_number), &mimir_magic_number,
		     sizeof(mimir_magic_number));
	if (err != 0) {
		debug("Can not load assessment: Wrong file type\n");
		err = -MIMIR_ERR_IO;
		goto out_close_file;
	}
	debug("password iterations: %d\n", pwd_iter_from_header(header));

	/*
	 * Derive key for decryption
	 */
	debug("derive decryption key from password\n");
	err = derive_key(password, strlen(password), header.password_salt,
			 sizeof(header.password_salt),
			 pwd_iter_from_header(header), key);
	if (err) {
		debug("Can not load assessment: Error deriving key\n");
		err = 1;
		goto out_close_file;
	}

	cipertext_len = 0;
	ciphertext = NULL;

	/*
	 * Read the file until EOF and store the complete ciphertext
	 * before decrypting
	 */
	while (!feof(fp)) {
		buf_len = fread(buf, sizeof(*buf), sizeof(buf), fp);
		tmp = realloc(ciphertext, (cipertext_len + buf_len) *
			      sizeof(*ciphertext));
		if (tmp) {
			ciphertext = tmp;
		} else {
			debug("Can not load assessment: Out of memory");
			err = -MIMIR_ERR_NOMEM;
			goto out_free_cipher;
		}
		memcpy(ciphertext + cipertext_len, buf, sizeof(*buf) * buf_len);
		cipertext_len += buf_len;
	}

	plaintext = malloc((cipertext_len - MIMIR_TAG_BYTES) *
			   sizeof(*plaintext));
	if (plaintext == NULL) {
		debug("Can not load assessment: Out of memory");
		err = -MIMIR_ERR_NOMEM;
		goto out_free_cipher;
	}

	err = decrypt(ciphertext, cipertext_len, (unsigned char *)&header,
		      sizeof(header), key, plaintext, &plaintext_len);
	if (err) {
		debug("Can not load assessment: Decryption failed!");
		err = 1;
		goto out_free_plain;
	}

	err = mimir_new(asmt);
	if (err) {
		debug("Can not load assessment: Can not create assessment");
		err = 1;
		goto out_free_plain;
	}
	err = deserialize_assessment(plaintext, plaintext_len, *asmt);
	if (err) {
		debug("Can not load assessment: Error on deserialization");
		err = 1;
		goto out_free_asmt;
	}

	/*
	 * Restore assessment and participant UUID from header and
	 * convert to host byte order.
	 */
	memcpy(&(*asmt)->asmt_uuid, header.asmt_uuid,
	       sizeof(struct mimir_uuid));
	(*asmt)->asmt_uuid = uuid_ntoh((*asmt)->asmt_uuid);

	memcpy(&(*asmt)->participant_uuid, header.participant_uuid,
	       sizeof(struct mimir_uuid));
	(*asmt)->participant_uuid = uuid_ntoh((*asmt)->participant_uuid);

	free(plaintext);
	free(ciphertext);
	fclose(fp);

	return 0;

 out_free_asmt:
	mimir_unref(*asmt);
 out_free_plain:
	free(plaintext);
 out_free_cipher:
	free(ciphertext);
 out_close_file:
	fclose(fp);
 out:
	return err;
 out_err_inval:
	return -MIMIR_ERR_INVAL;
}

/**
 * Return the start time of asmt
 *
 * Return the start time when the assessment was created in seconds
 * since 1970-01-01T00:00:00Z (unix epoch).
 *
 * @param asmt The mimir to get the time from
 * @return The time in seconds or ``-#MIMIR_ERR_INVAL``
 */
time_t mimir_get_time(const struct mimir * asmt)
{
	if (asmt == NULL)
		return -MIMIR_ERR_INVAL;
	else
		return asmt->time;
}

/*
 * Set participant identifier for asmt
 *
 * This set a user chooses participant identifier to the assessment.
 * The string will be copied into the asmt object and participant_id can
 * be freed after this.
 *
 * @param asmt The mimir to set the identifier
 * @param participant_id The NULL-terminated string as identifier
 * @return ``0`` on success, ``-#MIMIR_ERR_INVAL`` when invalid arguments
 * are passed or ``-#MIMIR_ERR_NOMEM`` when malloc() fails
 */
int mimir_set_pid(struct mimir *asmt, const char *participant_id)
{
	int ret;

	if (asmt == NULL)
		goto out;
	if (participant_id == NULL)
		goto out;

	asmt->participant_id = strdup(participant_id);
	if (asmt->participant_id == NULL) {
		debug("out of memory. malloc() failed");
		goto out_nomem;
	}

	ret = uuid_v5(&asmt->participant_uuid, mimir_uuid_namespace,
		      asmt->participant_id, strlen(asmt->participant_id));
	if (ret)
		return 1;

	return 0;

 out_nomem:
	return -MIMIR_ERR_NOMEM;
 out:
	return -MIMIR_ERR_INVAL;
}

/**
 * Get participant identifier form asmt
 *
 * Get the participant identifier from ``asmt``. ``buf`` must be large
 * enough to hold the identifiert. Setting buf to ``NULL`` will return
 * the needed space. No more than ``buf_len`` bytes will be written to
 * ``buf``
 *
 * @param asmt The mimir to get the identifier from
 * @param buf Buffer to store the identifier
 * @param buf_len Size of buffer
 * @return ``0`` on success, if ``buf == NULL`` the size needed for ``buf``
 * to store.
 */
int mimir_get_pid(const struct mimir *asmt, char *buf, const size_t buf_len)
{
	size_t len;

	if (asmt == NULL)
		goto out;
	len = strlen(asmt->participant_id) + 1;

	if (buf == NULL)
		return len;
	if (buf_len < len)
		len = buf_len;
	memcpy(buf, asmt->participant_id, len);

	return 0;
 out:
	return -MIMIR_ERR_INVAL;
}

/**
 * Check if libmimir has compiled with debugging options
 *
 * @return ``1`` when compiled with debugging, ``0`` when not
 */
int mimir_has_debug(void)
{
	return has_debug();
}

/**
 * Get assessment UUID from asmt
 *
 * Get the assessment UUID from the assessment as a string.
 * When ``buf`` is `NULL` the function will return the size of buf needed
 * to store the complete string, without trailing ``\0``.
 *
 * @param asmt The mimir_assessment to get the UUID from
 * @param buf Buffer to store the UUID
 * @param buf_len Size of the buffer
 * @return number of characters written to buf, or size needed for buf
 */
int mimir_get_uuid(const struct mimir *asmt, char *buf, const size_t buf_len)
{
	if (asmt == NULL)
		return -MIMIR_ERR_INVAL;
	if (buf == NULL)
		return UUID_STR_LEN - 1;

	return uuid_string(buf, buf_len, asmt->asmt_uuid);
}

/**
 * Get participant UUID from asmt
 *
 * Get the participant UUID from the assessment as a string.
 * When ``buf`` is `NULL` the function will return the size of buf needed
 * to store the complete string, without trailing ``\0``.
 *
 * @param asmt The mimir_assessment to get the UUID from
 * @param buf Buffer to store the UUID
 * @param buf_len Size of the buffer
 * @return number of characters written to buf, or size needed for buf
 */
int mimir_get_participant_uuid(const struct mimir *asmt, char *buf,
			       const size_t buf_len)
{
	if (asmt == NULL)
		return -MIMIR_ERR_INVAL;
	if (buf == NULL)
		return UUID_STR_LEN - 1;

	return uuid_string(buf, buf_len, asmt->participant_uuid);
}

static int create_ref_cards(struct mimir_card **c)
{
	int ret;
	struct mimir_card *card;
	int ref_card_flags[MIMIR_ASMT_REF_CARD_N] = {
		MIMIR_CARD_PROP_ONE | MIMIR_CARD_PROP_RED |
		    MIMIR_CARD_PROP_TRIANGLE,
		MIMIR_CARD_PROP_TWO | MIMIR_CARD_PROP_GREEN |
		    MIMIR_CARD_PROP_STAR,
		MIMIR_CARD_PROP_THREE | MIMIR_CARD_PROP_YELLOW |
		    MIMIR_CARD_PROP_CROSS,
		MIMIR_CARD_PROP_FOUR | MIMIR_CARD_PROP_BLUE |
		    MIMIR_CARD_PROP_CIRCLE
	};

	for (int i = 0; i < MIMIR_ASMT_REF_CARD_N; i++) {
		ret = card_new(&card, ref_card_flags[i]);
		if (ret)
			return ret;
		else
			c[i] = card;
	}

	return 0;
}

static void destroy_ref_cards(struct mimir_card **c)
{
	if (!c)
		return;
	for (int i = 0; i < MIMIR_ASMT_REF_CARD_N; i++) {
		card_unref_private(c[i]);
	}
}

/*
 * Check if reference card and the stimulus cards match the sort
 * principle.
 *
 * Returns '0' on failure, or non-zero on success.
 */
static int response_match_sort(struct mimir *asmt,
			       struct mimir_card *ref,
			       struct mimir_card *response)
{
	unsigned int ref_raw = mimir_card_get_prop_raw(ref);
	unsigned int response_raw = mimir_card_get_prop_raw(response);
	unsigned int ret = ref_raw & response_raw;
	const enum sort_principle principle = asmt->current_principle;

	switch (principle) {
	case SORT_FORM:
		ret &= 0x0F0;
		debug("match form: %s\n", ret ? "yes" : "no");
		break;
	case SORT_COLOR:
		ret &= 0xF00;
		debug("match color: %s\n", ret ? "yes" : "no");
		break;
	case SORT_NUMBER:
		ret &= 0x00F;
		debug("match number: %s\n", ret ? "yes" : "no");
		break;
	case SORT_N:
	default:
		ret = 0;
		break;
	}

	return ret;
}

/*
 * Check if the partipicant had 10 consecutive correct answers
 * switch to the next sort principle. When the last principle is
 * reached, start again at first principle.
 */
static void switch_sort_principle(struct mimir *asmt)
{
	if (asmt->con_correct == 10) {
		asmt->current_principle++;
		asmt->con_correct = 0;
		debug("switch to next sort principle\n");
	}
	if (asmt->current_principle == SORT_N) {
		asmt->current_principle = SORT_COLOR;
		asmt->sequences_passed++;
		debug("sequence passed, start at first sort principle\n");
	}
}

static int asmt_reaches_end(struct mimir *asmt)
{
	if (asmt->cards_used == MIMIR_N_CARDS * 2) {
		debug("end condition 'all cards used' reached\n");
		asmt->finished = 1;
	}
	if (asmt->sequences_passed == 2) {
		debug("end condition 'two sequences passed' reached\n");
		asmt->finished = 1;
	}

	return asmt->finished;
}

/*
 * Create Mimir file header
 *
 * Create a struct mimir_header ready to write to file. The arguments
 * isotime and password_iterations are written into the header.
 *
 * When password_iterations == 0, then the default value is used.
 */
static struct mimir_header make_header(const unsigned int password_iterations,
				       const struct mimir_uuid asmt_uuid,
				       const struct mimir_uuid participant_uuid)
{
	unsigned int iter;
	struct mimir_header header;
	struct mimir_uuid net_uuid;

	memset(&header, 0, sizeof(header));

	memcpy(&header, &mimir_magic_number, sizeof(mimir_magic_number));
	header.file_version = MIMIR_FILE_VERSION;
	get_random_bytes(&(header.password_salt), sizeof(header.password_salt));

	if (password_iterations == 0)
		iter = MIMIR_PASSWORD_ITERATIONS;
	else
		iter = password_iterations;

	/* Save in network byte order */
	iter = htonl(iter);
	memcpy(&(header.password_iterations), &iter,
	       sizeof(header.password_iterations));

	net_uuid = asmt_uuid;
	net_uuid = uuid_hton(net_uuid);
	memcpy(&header.asmt_uuid, &net_uuid, sizeof(header.asmt_uuid));

	net_uuid = uuid_hton(participant_uuid);
	memcpy(&header.participant_uuid, &net_uuid,
	       sizeof(header.participant_uuid));

	return header;
}

/*
 * Returns the password iteration from the header in correct byte order
 */
static inline unsigned int pwd_iter_from_header(const struct mimir_header
						header)
{
	unsigned int iter;

	memcpy(&iter, &(header.password_iterations),
	       sizeof(header.password_iterations));

	return ntohl(iter);
}
