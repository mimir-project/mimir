/* random.h
 *
 * Defines to a kernel CSPNRG and a fallback user-land
 * random number source
 *
 * Copyright (C) 2018 Jens Sauer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef LIBMIMIR_INSIDE
#error "You can not include random.h in the public header libmimir.h"
#endif

#include <stddef.h>

#ifndef LIBMIMIR_RANDOM_H
#define LIBMIMIR_RANDOM_H

/*
 * Get random bytes from a kernel souce
 *
 * TODO: support more OS than Linux
 */

/**
 * Get size bytes of random data form kernel
 *
 * The maximum size of random bytes are 256.
 *
 * @param out Address to store random bytes
 * @param size Number of random bytes
 * @return ``0`` on success ``1`` on failure
 */
#define get_random_bytes(out, size) get_random_bytes_os(out, size)

#ifdef __linux__
#define get_random_bytes_os(out, size) get_random_bytes_linux(out, size)
int get_random_bytes_linux(void *out, const size_t len);

#else
#error Your OS CSPRNG is currently not supported!

#endif

#endif				/* LIBMIMIR_RANDOM_H */
