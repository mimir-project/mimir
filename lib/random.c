/* random.c
 *
 * This file implements access to a kernel CSPNRG and a fallback user-land
 * random number source
 *
 * Copyright (C) 2018 Jens Sauer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include "random.h"

#ifdef __linux__
#include <unistd.h>
#include <sys/syscall.h>
#else
#include <openssl/rand.h>
#endif

#ifdef __linux__
/**
 * Put len bytes of random data in out
 *
 * Linux kernel as CSPRNG. The maximum size of len is 256.
 *
 * @param out Address where to store random bytes
 * @param len Number of random bytes
 * @return ``0`` on success, ``1`` on failure
 */
int get_random_bytes_linux(void *out, const size_t len)
{
	size_t ret;

	ret = syscall(SYS_getrandom, out, len, 0);

	return ret < len ? 1 : 0;
}
#endif
