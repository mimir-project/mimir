/* card.h
 *
 * Copyright (C) 2019 Jens Sauer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef LIBMIMIR_INSIDE
#error "You can not include card.h in the public header libmimir.h"
#endif

#ifndef LIBMIMIR_CARD_H
#define LIBMIMIR_CARD_H

/**
 * Opaque type which represents a card in libmimir
 */
struct mimir_card {
	/**
	 * Card properties, combination of three #mimir_card_property values
	 */
	unsigned int property_flags;
	/**
	 * Private reference counter. Mostly referenced by mimir_card_list
	 */
	unsigned int private_ref_count;
	/**
	 * Public reference counter. Cards which are passed to the user will
	 * have an increased public counter.
	 */
	unsigned int public_ref_count;
};

/*
 * Private mimir_card prototypes
 */
int card_new(struct mimir_card **card, const unsigned int properties);
void card_unref_private(struct mimir_card *card);
struct mimir_card *card_ref_private(struct mimir_card *card);

#endif				/* LIBMIMIR_CARD_H */
