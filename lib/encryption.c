/* encryption.c
 *
 * Implementation of mimir encryption functions
 *
 * Copyright (C) 2018 Jens Sauer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <openssl/evp.h>
#include <libmimir.h>
#include "libmimir-private.h"

/**
 * Derive MIMIR_KEY_BYTES long key from password
 *
 * PBKDF2 with SHA-512 is used to derive a key from password.
 *
 * @param password Password string
 * @param password_len Length of password
 * @param salt Salt for PBKDF2
 * @param salt_len Length of salt
 * @param password_iterations Number of PBKDF2 iterations
 * @param key Destination for key
 * @return ``0`` on success, ``1`` on failure
 */
int derive_key(const char *password, const size_t password_len,
	       const unsigned char *salt, const size_t salt_len,
	       const unsigned int password_iterations,
	       unsigned char key[MIMIR_KEY_BYTES])
{
	int ret;

	ret = PKCS5_PBKDF2_HMAC(password, password_len, salt, salt_len,
				password_iterations, EVP_sha512(),
				MIMIR_KEY_BYTES, key);

	return ret ? 0 : 1;
}

/**
 * Encrypts data with ChaCha20-Poly1305
 *
 * Encrypts plaintext_len bytes of buffer plaintext and put it into
 * ciphertext buffer. Make sure there is enough space.
 *
 * The nonce will always be ``0``, make sure the key is unique.
 *
 * @param plaintext Plaintext buffer
 * @param plaintext_len Size to read from plaintext
 * @param ad Additional data
 * @param ad_len Length of additonal data
 * @param key Key to use for encryption
 * @param ciphertext Buffer to store ciphertext
 * @param ciphertext_len Size of written ciphertext
 * @return ``0`` on success, ``1`` on error
 */
int encrypt(const unsigned char *plaintext, const size_t plaintext_len,
	    const unsigned char *ad, const size_t ad_len,
	    const unsigned char key[MIMIR_KEY_BYTES],
	    unsigned char *ciphertext, size_t * ciphertext_len)
{
	int ret;
	int len;
	EVP_CIPHER_CTX *ctx;

	ctx = EVP_CIPHER_CTX_new();
	if (ctx == NULL)
		goto out;
	/*
	 * The key will never be used a second time, it it safe to
	 * set the ChaCha20-Poly1305 nonce to zero
	 */
	ret = EVP_EncryptInit(ctx, EVP_chacha20_poly1305(), key, 0);
	if (ret == 0)
		goto out_free_ctx;

	/* Add additional data */
	ret = EVP_EncryptUpdate(ctx, NULL, &len, ad, ad_len);
	if (ret == 0)
		goto out_free_ctx;

	ret = EVP_EncryptUpdate(ctx, ciphertext, &len, plaintext,
				plaintext_len);
	if (ret == 0)
		goto out_free_ctx;
	*ciphertext_len = len;

	ret = EVP_EncryptFinal(ctx, ciphertext + len, &len);
	if (ret == 0)
		goto out_free_ctx;
	*ciphertext_len += len;

	/* Get AEAD tag  */
	ret = EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_AEAD_GET_TAG, MIMIR_TAG_BYTES,
				  ciphertext + *ciphertext_len);
	if (ret == 0)
		goto out_free_ctx;
	*ciphertext_len += MIMIR_TAG_BYTES;

	EVP_CIPHER_CTX_free(ctx);

	return 0;

 out_free_ctx:
	EVP_CIPHER_CTX_free(ctx);
 out:
	return 1;
}

/**
 * Decrypts data with ChaCha20-Poly1305
 *
 * Decrypts ciphertext_len bytes of buffer ciphertext and put it into
 * plaintext buffer. When the decryption fails, decrypt() will fail and set
 * plaintext_len to ``0``
 *
 * @param plaintext Plaintext buffer
 * @param plaintext_len Size written to plaintext
 * @param ad Additional data
 * @param ad_len Length of additonal data
 * @param key Key to use for decryption
 * @param ciphertext Ciphertext buffer
 * @param ciphertext_len Size of ciphertext buffer
 * @return ``0`` on success, ``1`` on error
 */
int decrypt(const unsigned char *ciphertext, const size_t ciphertext_len,
	    const unsigned char *ad, const size_t ad_len,
	    const unsigned char key[MIMIR_KEY_BYTES],
	    unsigned char *plaintext, size_t * plaintext_len)
{
	int ret;
	int len;
	EVP_CIPHER_CTX *ctx;

	ctx = EVP_CIPHER_CTX_new();
	if (ctx == NULL)
		goto out;
	/*
	 * The key will never be used a second time, it it safe to
	 * set the ChaCha20-Poly1305 nonce to zero
	 */
	ret = EVP_DecryptInit(ctx, EVP_chacha20_poly1305(), key, 0);
	if (ret == 0)
		goto out_free_ctx;

	/* Provide AEAD tag before any EVP_DecryptUpdate() calls */
	ret = EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_AEAD_SET_TAG, MIMIR_TAG_BYTES,
				  (void *)(ciphertext + ciphertext_len -
					   MIMIR_TAG_BYTES));
	if (ret == 0)
		goto out_free_ctx;

	/* Add additional data */
	ret = EVP_DecryptUpdate(ctx, NULL, &len, ad, ad_len);
	if (ret == 0)
		goto out_free_ctx;

	ret = EVP_DecryptUpdate(ctx, plaintext, &len, ciphertext,
				ciphertext_len - MIMIR_TAG_BYTES);
	if (ret == 0)
		goto out_null_len;
	*plaintext_len = len;

	ret = EVP_DecryptFinal(ctx, plaintext + len, &len);
	if (ret == 0)
		goto out_null_len;
	*plaintext_len += len;

	EVP_CIPHER_CTX_free(ctx);

	return 0;

 out_null_len:
	*plaintext_len = 0;
 out_free_ctx:
	EVP_CIPHER_CTX_free(ctx);
 out:
	return 1;
}
