/* serialize.c
 *
 * Serialize and deserialze a Mimir Assessment
 *
 * Copyright (C) 2018 Jens Sauer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <libmimir.h>
#include <string.h>
#include <arpa/inet.h>
#include <endian.h>
#include "libmimir-private.h"

#define CHUNK_TYPE_BYTES 4
#define CHUNK_TYPE_ASMT 1
#define CHUNK_TYPE_TIME 2
#define CHUNK_TYPE_IDPA 3
#define CHUNK_TYPE_AFIN 4

struct data_chunk_header {
	uint32_t length;
	uint8_t type[CHUNK_TYPE_BYTES];
};

const uint8_t chunk_type_asmt[] = { 0x41, 0x53, 0x4d, 0x54 };
const uint8_t chunk_type_time[] = { 0x84, 0x73, 0x77, 0x69 };
const uint8_t chunk_type_idpa[] = { 0x49, 0x44, 0x50, 0x41 };
const uint8_t chunk_type_afin[] = { 0x41, 0x46, 0x49, 0x4e };

static int get_chunk_id(const uint8_t type[CHUNK_TYPE_BYTES]);

static int serialize_response_list(unsigned char *out, const size_t out_len,
				   const struct response_list *list);

static int deserialize_response_list(unsigned char *in,
				     const size_t in_len,
				     struct response_list *list);

static void serialize_time(unsigned char *out, const time_t time);
static int deserialize_time(const unsigned char *in, const size_t in_len,
			    time_t * time);
static size_t get_time_size(void);

static size_t get_response_list_size(const struct response_list *list);

static void serialize_participant_id(unsigned char *out, const size_t out_len,
				     const char *participant_id);
static int deserialize_participant_id(unsigned char *in, const size_t in_len,
				      char **participant_id);
static size_t get_participant_id_size(const char *participant_id);

static void serialize_afin_chunk(unsigned char *out, const size_t out_len,
				 const int is_finished);
static int deserialize_afin_chunk(const unsigned char *in, const size_t in_len,
				  int *is_finished);
static size_t get_afin_chunk_size(void);

/*
 * Serialize a mimir assessment
 *
 * Serialize asmt into consecutive bytes to store into file.
 *
 * TODO: Add missing chunk types
 */
int serialize_assessment(unsigned char **out, size_t * out_len,
			 const struct mimir *asmt)
{
	unsigned char *buf;
	unsigned char *tmp;
	size_t len;
	int ret;

	len = get_response_list_size(asmt->response_list);
	buf = malloc(len * sizeof(unsigned char));
	if (buf == NULL) {
		ret = -MIMIR_ERR_NOMEM;
		goto out;
	}
	ret = serialize_response_list(buf, len, asmt->response_list);
	if (ret) {
		ret = 1;
		goto out_free_buf;
	}
	*out_len = len;

	/* TIME Chunk */
	len = get_time_size();
	tmp = realloc(buf, *out_len + len);
	if (tmp == NULL) {
		ret = -MIMIR_ERR_NOMEM;
		goto out_free_buf;
	}
	buf = tmp;
	serialize_time(buf + *out_len, asmt->time);
	*out_len += len;

	/* Skip if no participant_id is set */
	if (asmt->participant_id != NULL) {
		len = get_participant_id_size(asmt->participant_id);
		tmp = realloc(buf, *out_len + len);
		if (tmp == NULL) {
			ret = -MIMIR_ERR_NOMEM;
			goto out_free_buf;
		}
		buf = tmp;
		serialize_participant_id(buf + *out_len, len,
					 asmt->participant_id);
		*out_len += len;
	}
	/* AFIN chunk */
	len = get_afin_chunk_size();
	tmp = realloc(buf, *out_len + len);
	if (tmp == NULL) {
		ret = -MIMIR_ERR_INVAL;
		goto out_free_buf;
	}
	buf = tmp;
	serialize_afin_chunk(buf + *out_len, len, asmt->finished);
	*out_len += len;

	*out = buf;

	return 0;

 out_free_buf:
	free(buf);
 out:
	return ret;
}

/*
 * Deserialize a mimir assessment
 *
 * Deserialize data in ``in`` into asmt
 *
 * TODO: Add missing chunk types
 */
int deserialize_assessment(const unsigned char *in, const size_t in_len,
			   struct mimir *asmt)
{
	struct data_chunk_header header;
	unsigned char *buf;
	size_t pos;
	int err;

	pos = 0;
	while (pos < in_len) {
		memcpy(&header, in + pos, sizeof(header));
		header.length = ntohl(header.length);

		buf = malloc(header.length * sizeof(unsigned char));
		if (buf == NULL) {
			err = -MIMIR_ERR_NOMEM;
			goto out;
		}
		pos += sizeof(header);
		memcpy(buf, in + pos, header.length * sizeof(unsigned char));
		pos += header.length;

		switch (get_chunk_id(header.type)) {
		case CHUNK_TYPE_ASMT:
			err = deserialize_response_list(buf, header.length,
							asmt->response_list);
			break;
		case CHUNK_TYPE_TIME:
			err = deserialize_time(buf, header.length,
					       &(asmt->time));
			break;
		case CHUNK_TYPE_IDPA:
			err = deserialize_participant_id(buf, header.length,
							 &(asmt->
							   participant_id));
			break;
		case CHUNK_TYPE_AFIN:
			err = deserialize_afin_chunk(buf, header.length,
						     &(asmt->finished));
			break;
			/* Skip unknown chunk types */
		default:
			debug("Unknown chunk type '%c%c%c%c' skipping chunk\n",
			      header.type[0], header.type[1],
			      header.type[2], header.type[3]);
			err = 0;
			break;
		}
		if (err)
			goto out_free_buf;

		free(buf);
	};

	return 0;

 out_free_buf:
	free(buf);
 out:
	return err;
}

/*
 * Serialize response_list list into out
 *
 * This will serialize the response_list into the 'ASMT' chunk type.
 * No more than out_len bytes will be written
 *
 * Returns ``0`` on success, ``1`` when buffer is to small
 */
static int serialize_response_list(unsigned char *out, const size_t out_len,
				   const struct response_list *list)
{
	struct data_chunk_header header;
	struct response_node *head;
	struct response_data *data;
	struct response_data tmp;
	size_t len;

	head = list->head;
	header.length = htonl(sizeof(struct response_data));
	memcpy(&(header.type), chunk_type_asmt, sizeof(header.type));
	len = 0;

	while (response_list_walk(&head, &data)) {
		if (len + sizeof(header) > out_len)
			goto out;
		memcpy(out + len, &header, sizeof(header));
		len += sizeof(header);

		if (len + sizeof(struct response_data) > out_len)
			goto out;

		tmp = response_data_hton(*data);

		memcpy(out + len, &tmp, sizeof(struct response_data));
		len += sizeof(struct response_data);
	};
	return 0;
 out:
	return 1;
}

/*
 * Deserialize 'ASMT' chunk type into response_list data
 *
 * Returns ``0`` on success, ``1`` when chunk size is incorrect or
 *   -MIMIR_ERR_NOMEM when no memory was available
 */
static int deserialize_response_list(unsigned char *in,
				     const size_t in_len,
				     struct response_list *list)
{
	int err;
	struct response_data data;

	/* Size missmatch, should not happen! */
	if (sizeof(data) != in_len) {
		err = 1;
		goto out;
	}
	memcpy(&data, in, sizeof(data));
	data = response_data_ntoh(data);

	err = response_list_append_data(list, &data);
	if (err)
		goto out;

	return 0;
 out:
	return err;
}

static size_t get_response_list_size(const struct response_list *list)
{
	return response_list_nodes(list) * (sizeof(struct response_data) +
					    sizeof(struct data_chunk_header));
}

static int get_chunk_id(const uint8_t type[CHUNK_TYPE_BYTES])
{
	if (memcmp(type, chunk_type_asmt, CHUNK_TYPE_BYTES) == 0)
		return CHUNK_TYPE_ASMT;
	if (memcmp(type, chunk_type_time, CHUNK_TYPE_BYTES) == 0)
		return CHUNK_TYPE_TIME;
	if (memcmp(type, chunk_type_idpa, CHUNK_TYPE_BYTES) == 0)
		return CHUNK_TYPE_IDPA;
	if (memcmp(type, chunk_type_afin, CHUNK_TYPE_BYTES) == 0)
		return CHUNK_TYPE_AFIN;

	return 0;
}

/*
 * Return length for 'IDPA' chunk
 */
static size_t get_participant_id_size(const char *participant_id)
{
	return sizeof(struct data_chunk_header) + strlen(participant_id) + 1;
}

/*
 * Serialize member of struct mimir_assessment.participant_id into
 * 'IDPA' chunk type
 */
static void serialize_participant_id(unsigned char *out, const size_t out_len,
				     const char *participant_id)
{
	struct data_chunk_header header;
	size_t len;

	len = out_len - sizeof(header);
	memcpy(&(header.type), chunk_type_idpa, CHUNK_TYPE_BYTES);
	header.length = ntohl(len);

	memcpy(out, &header, sizeof(header));
	memcpy(out + sizeof(header), participant_id, len);
}

/*
 * Deserialize a 'IDPA' chunk type into participant_id member of
 * struct mimir_assessment
 */
static int deserialize_participant_id(unsigned char *in, const size_t in_len,
				      char **participant_id)
{
	*participant_id = malloc(in_len * sizeof(char));
	if (*participant_id == NULL)
		return -MIMIR_ERR_NOMEM;
	memcpy(*participant_id, in, in_len);

	return 0;
}

/*
 * Serialize assessment time into 'TIME' chunk
 */
static void serialize_time(unsigned char *out, const time_t time)
{
	struct data_chunk_header header;
	int64_t tmp = htobe64(time);

	memcpy(&(header.type), chunk_type_time, CHUNK_TYPE_BYTES);
	header.length = htonl(sizeof(tmp));
	memcpy(out, &header, sizeof(header));
	memcpy(out + sizeof(header), &tmp, sizeof(tmp));
}

/*
 * Deserializes 'TIME' chunk into assessment time
 */
static int deserialize_time(const unsigned char *in, const size_t in_len,
			    time_t * time)
{
	int64_t tmp;

	memcpy(&tmp, in, in_len);
	*time = be64toh(tmp);

	return 0;
}

/*
 * Returns the chuck size for 'TIME' chunk
 */
static size_t get_time_size(void)
{
	return sizeof(struct data_chunk_header) + sizeof(int64_t);
}

/*
 * Serializes struct mimir_assessment member 'finished' into 'AFIN' chunk
 */
static void serialize_afin_chunk(unsigned char *out, const size_t out_len,
				 const int is_finished)
{
	struct data_chunk_header header;
	int32_t tmp;

	memcpy(&(header.type), chunk_type_afin, CHUNK_TYPE_BYTES);
	header.length = htonl(sizeof(int32_t));
	tmp = htonl(is_finished);

	memcpy(out, &header, sizeof(header));
	memcpy(out + sizeof(header), &tmp, sizeof(int32_t));
}

/*
 * Deserialized 'AFIN' chunk
 */
static int deserialize_afin_chunk(const unsigned char *in, const size_t in_len,
				  int *is_finished)
{
	memcpy(is_finished, in, in_len);
	*is_finished = ntohl(*is_finished);

	return 0;
}

/*
 * Get 'AFIN' chunk size
 */
static size_t get_afin_chunk_size(void)
{
	return sizeof(struct data_chunk_header) + sizeof(int32_t);
}
