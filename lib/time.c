/* time.c
 *
 * Platform independent implementations of mimir_time_diff()
 *
 * Copyright (C) 2019 Jens Sauer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libmimir.h>
#include <math.h>

int mimir_time_diff(const struct mimir_time mt)
{
	double diff;

	diff = (mt.end.tv_sec - mt.start.tv_sec) * 1000 +
	    (double)(mt.end.tv_nsec - mt.start.tv_nsec) / 1000000;

	return round(diff);
}
