/* response.h
 *
 * List of responses
 *
 * Copyright (C) 2018 Jens Sauer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef LIBMIMIR_INSIDE
#error "You can not include response.h in the public header libmimir.h"
#endif

#include <inttypes.h>

#ifndef LIBMIMIR_RESPONSE_H
#define LIBMIMIR_RESPONSE_H

/**
 * Serialized data of a response.
 * Remeber all values are in network byte order
 */
struct response_data {
	/** SHA-256 hash of data excluding this value */
	uint8_t hash[32];
	/** SHA-256 hash of previous data block */
	uint8_t parent_hash[32];
	uint32_t refcard_raw;
	uint32_t stimcard_raw;
	uint32_t sort_principle;
	uint32_t correct;
	int32_t time_msec;
};

/**
 * Internal response node
 */
struct response_node {
	struct response_node *next;
	struct response_node *previous;
	struct response_data data;
};

/**
 * Internal list of responses
 */
struct response_list {
	struct mimir *asmt;
	struct response_node *head;
	struct response_node *tail;
};

int response_list_new(struct response_list **list, struct mimir *asmt);

void response_list_free(struct response_list *list);

int response_list_append(struct response_list *list,
			 struct mimir_card *ref_card,
			 struct mimir_card *stim_card,
			 const unsigned int sort_principle,
			 const unsigned int correct_response,
			 const int time_msec);

int response_list_append_data(struct response_list *list,
			      const struct response_data *data);

size_t response_list_nodes(const struct response_list *list);

int response_list_walk(struct response_node **head,
		       struct response_data **data);

int response_data_hash(struct response_data *data);
struct response_data response_data_hton(const struct response_data data);
struct response_data response_data_ntoh(const struct response_data data);

#endif				/* LIBMIMIR_RESPONSE_H */
