/* debug.c
 *
 * Write debug messages to stderr
 *
 * Copyright (C) 2019 Jens Sauer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#ifdef HAVE_SECURE_GETENV
#define _GNU_SOURCE
#endif

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <assert.h>
#include <libmimir.h>
#include "libmimir-private.h"

/*
 * Write debug messages to stderr when environment variable
 * MIMIR_DEBUG is set.
 */
void debug_write(const char *file, int line, const char *format, ...)
{
	va_list args;
	const char *env;
	char buf[256];
	int err;

	/*
	 * Check if debug messages are exceeding the limit. Even if
	 * they are not shown at this call. This will vanish in
	 * production code so this extra costs are not a problem.
	 */
	va_start(args, format);
	err = vsnprintf(buf, sizeof(buf), format, args);
	assert(err < (int)sizeof(buf));
	va_end(args);

	env = secure_getenv("MIMIR_DEBUG");
	if (env == NULL)
		return;

	fprintf(stderr, "%s:%s:%d: %s\n", PACKAGE, file, line, buf);
}
