/* mimir-response.c
 *
 * List of responses
 *
 * Copyright (C) 2018 Jens Sauer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <openssl/sha.h>
#include <openssl/evp.h>
#include <libmimir.h>
#include "libmimir-private.h"

/**
 * Create response_list list
 *
 * Create a new response_list to track the responses of the assessment
 *
 * @param list Address where to store list
 * @param asmt The parent struct mimir_assessment
 * @return ``0`` on success or ``-#MIMIR_ERR_NOMEM`` when malloc() failed
 */
int response_list_new(struct response_list **list, struct mimir *asmt)
{
	struct response_list *response_list;

	response_list = malloc(sizeof(struct response_list));
	if (response_list == NULL) {
		debug("Out of memory\n");
		goto out;
	}
	response_list->asmt = asmt;
	response_list->head = NULL;
	response_list->tail = NULL;

	*list = response_list;

	return 0;

 out:
	*list = NULL;
	return -MIMIR_ERR_NOMEM;
}

/**
 * Free response_list list
 *
 * Free all nodes and data of list. When list is ``NULL`` nothing will
 * happen.
 *
 * @param list response_list to free
 */
void response_list_free(struct response_list *list)
{
	struct response_node *node;

	if (list == NULL)
		return;

	while (list->head) {
		node = list->head->next;
		free(list->head);
		list->head = node;
	}
	free(list);
}

/**
 * Append data to response_list list
 *
 * Append the data to the end of list. Each data can be identified by
 * a unique SHA-256 hash.
 *
 * @param list The response_list to append data
 * @param ref_card mimir_card which was the reference card
 * @param stim_card mimir_card which was the stimulus card
 * @param sort_principle Sorting principle which was active
 * @param correct_response Is > 0 when the response was correct
 *
 * @return ``0`` or ``-#MIMIR_ERR_NOMEM`` when malloc() failed
 */
int response_list_append(struct response_list *list,
			 struct mimir_card *ref_card,
			 struct mimir_card *stim_card,
			 const unsigned int sort_principle,
			 const unsigned int correct_response,
			 const int time_msec)
{
	struct response_node *node;

	node = malloc(sizeof(struct response_node));
	if (node == NULL) {
		debug("Out of memory!\n");
		goto out;
	}
	if (list->head == NULL) {
		node->previous = NULL;
		list->head = node;
		list->tail = node;
	} else {
		list->tail->next = node;
		node->previous = list->tail;
		list->tail = node;
	}
	node->next = NULL;
	node->data.correct = correct_response;
	node->data.sort_principle = sort_principle;
	node->data.stimcard_raw = mimir_card_get_prop_raw(stim_card);
	node->data.refcard_raw = mimir_card_get_prop_raw(ref_card);
	node->data.time_msec = time_msec;

	if (node->previous == NULL)
		SHA256(NULL, 0, node->data.parent_hash);
	else
		memcpy(node->data.parent_hash, node->previous->data.hash,
		       SHA256_DIGEST_LENGTH);

	return response_data_hash(&node->data);

 out:
	return -MIMIR_ERR_NOMEM;
}

/**
 * Create SHA-256 hash of data
 *
 * The hash is created by converting all multi-byte types to network
 * byte order and hash them in the order of members.
 *
 * @param data The response_data to hash
 * @return ``0`` on success ``1`` on failure
 */
int response_data_hash(struct response_data *data)
{
	EVP_MD_CTX *ctx;
	struct response_data tmp;
	int err;

	ctx = EVP_MD_CTX_new();
	if (ctx == NULL)
		return 1;
	err = EVP_DigestInit_ex(ctx, EVP_sha256(), NULL);
	if (err == 0)
		goto out;

	tmp = response_data_hton(*data);

	/*
	 * Skip first 32 byte which will store the hash
	 * we are about to create.
	 */
	err = EVP_DigestUpdate(ctx, &tmp + SHA256_DIGEST_LENGTH,
			       sizeof(tmp) - SHA256_DIGEST_LENGTH);
	if (err == 0)
		goto out;

	err = EVP_DigestFinal_ex(ctx, data->hash, NULL);
	if (err == 0)
		goto out;

	EVP_MD_CTX_free(ctx);

	return 0;

 out:
	EVP_MD_CTX_free(ctx);
	return 1;
}

/**
 * Append precreated data into list
 *
 * Append data to the end of list. In contrast to response_list_append()
 * this function need a precreated response_data object.
 *
 * @param list The response_list to append data
 * @param data The response_data to append into list
 *
 * @return ``0`` or ``-#MIMIR_ERR_NOMEM`` when malloc() fails
 */
int response_list_append_data(struct response_list *list,
			      const struct response_data *data)
{
	struct response_node *node;

	node = malloc(sizeof(struct response_node));
	if (node == NULL) {
		debug("Out of memory!\n");
		goto out;
	}
	if (list->head == NULL) {
		node->previous = NULL;
		list->head = node;
		list->tail = node;
	} else {
		list->tail->next = node;
		node->previous = list->tail;
		list->tail = node;
	}
	node->next = NULL;
	memcpy(&(node->data), data, sizeof(struct response_data));

	return 0;
 out:
	return -MIMIR_ERR_NOMEM;
}

/**
 * Get count of list nodes
 *
 * @param list The response_list to count
 * @return Number of nodes
 */
size_t response_list_nodes(const struct response_list * list)
{
	size_t n;
	struct response_node *node;

	n = 0;
	node = list->head;
	while (node) {
		n++;
		node = node->next;
	}
	return n;
}

/**
 * Walk response_list using response_node
 *
 * Returns the data of head and set head to next element
 *
 * @param head Current response_node
 * @param data Data of head
 * @return ``1`` if succeeded or ``0`` when end is reached
 */
int response_list_walk(struct response_node **head, struct response_data **data)
{
	if (*head == NULL)
		return 0;

	*data = &((*head)->data);
	*head = (*head)->next;

	return 1;
}

/**
 * Convert data to network byte order
 *
 * Converts response_data into network byte order.
 *
 * @param data The response_data to convert.
 * @return data in network byte order
 */
struct response_data response_data_hton(const struct response_data data)
{
	struct response_data tmp;

	tmp = data;
	tmp.refcard_raw = htonl(tmp.refcard_raw);
	tmp.stimcard_raw = htonl(tmp.stimcard_raw);
	tmp.sort_principle = htonl(tmp.sort_principle);
	tmp.correct = htonl(tmp.correct);
	tmp.time_msec = htonl(tmp.time_msec);

	return tmp;
}

/**
 * Convert data to host byte order
 *
 * Converts response_data into host byte order.
 *
 * @param data The response_data to convert.
 * @return data in host byte order
 */
struct response_data response_data_ntoh(const struct response_data data)
{
	struct response_data tmp;

	tmp = data;
	tmp.refcard_raw = ntohl(tmp.refcard_raw);
	tmp.stimcard_raw = ntohl(tmp.stimcard_raw);
	tmp.sort_principle = ntohl(tmp.sort_principle);
	tmp.correct = ntohl(tmp.correct);
	tmp.time_msec = ntohl(tmp.time_msec);

	return tmp;
}
