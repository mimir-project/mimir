Contribution Guide
==================

Make a distribution tarball
---------------------------
First run `configure --enable-doc` otherwise `make dist` will fail.
Run `make docs distcheck` to make a distribution tarball. The target `docs` is
needed to include the latest docs.

Coding style
------------
The preferred coding style for libmimir is oriented towards the Linux kernel
coding style:  
See <https://www.kernel.org/doc/html/latest/process/coding-style.html> for more
details.

You can use `tools/Lindent` to fix source code formatting.
